#Autor: Sergio Perez Sanchez

Feature: Login

	Scenario: Identificacion Valida
		Given The user has registered in the system
		When The user inserts a valid identification
		Then The user will be granted access to the system 
	
	Scenario: Identificacion Invalida
		Given the user has registered in the system
		When the user inserts a wrong identification
		Then The user will not be granted access to the system
		