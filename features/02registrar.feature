#Autor: Sergio Perez Sanchez

Feature: Registro

	Scenario: Registro Correcto
		Given The user is not registered in the database
		When The user gives a name and a password 
		Then The user will be registered
		
	Scenario: Registro Incorrecto - Cuenta Existente
		Given Un usuario se quiere registrarse
		When Se quiere registrar con una cuenta existente
		Then No se registra	
		
	Scenario: Registro Incorrecto - Clave Menos Ocho Caracteres
		Given Un usuario se va a registrar
		When Se registra con una clave de menos de ocho caracteres
		Then No se registra	