<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//ES" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <!— Latest compiled and minified CSS —>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!— Optional theme —>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  
  <link href="https://mottie.github.io/tablesorter/docs/css/prettify.css" rel="stylesheet">
  <link href="https://mottie.github.io/tablesorter/css/theme.grey.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../css/estilos.css" />
  
  <!— Latest compiled and minified JavaScript —>
  <script src="http://code.jquery.com/jquery-latest.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript" src="https://mottie.github.io/tablesorter/js/jquery.tablesorter.js"></script>
  <script type="text/javascript" src="https://mottie.github.io/tablesorter/js/jquery.tablesorter.widgets.js"></script> 

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TodoAPP - Home Admin</title>

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">
</head>

<style type="text/css">
    .button_active {
        background:black;
        cursor:pointer;
        border:none;
        height: 50px;
        position: relative;
        color: grey;
    }
    .button {
        background:none;
        cursor:pointer;
        border:none;
        height: 50px;
        position: relative;
        color: grey;
    }

    .button:hover {color: #FFFFFF}
    .button_active:hover {color: #FFFFFF}
    
</style>
  
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="https://sites.google.com/view/todoapp-sps/p%C3%A1gina-principal">TodoApp</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
    
      <ul class="nav navbar-nav">
        <li class="active">
        <form action="HomeAdmin.htm" method="post" id="menuPrincipalAdminHome">
        <button class="button_active" type="submit" id="btnHomeAdmin" disabled>
          <span class="glyphicon glyphicon-home" aria-hidden="true" disabled></span>
            Principal  
          <span class="sr-only"> (current)</span>
          </button>
         </form>
        </li>
        
        <li class="dropdown">
        <button class="dropdown-toggle button" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" id="btnGestionUsuarios">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Gestion de Usuarios <span class="caret"></span> 
        </button>
        <ul class="dropdown-menu">
          <li>
          <form action="AltasUsuarios.htm" method="post" id="menuPrincipalAdminAltasUsuarios">
            <button class="button" type="submit" id="btnDarAltas">
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span><span class="glyphicon glyphicon-user" aria-hidden="true"></span>
              Dar de Alta
            </button>
           </form>
          </li>
          <li>
          <form action="BajasUsuarios.htm" method="post" id="menuPrincipalAdminBajasUsuarios">
            <button class="button" type="submit" id="btnDarBajas">
            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span><span class="glyphicon glyphicon-user" aria-hidden="true"></span>
              Dar de Baja
            </button>
           </form>
          </li>
          <li role="separator" class="divider"></li>
          <li>
          <form action="CambiarRoles.htm" method="post" id="menuPrincipalAdminCambiarRoles">
            <button class="button" type="submit" id="btnCambiarRoles">
              <span class="glyphicon glyphicon-transfer" aria-hidden="true"></span><span class="glyphicon glyphicon-user" aria-hidden="true"></span>
              Cambiar Roles
            </button>
           </form>
          </li>
          <li role="separator" class="divider"></li>
        </ul>
      </li>
      
      <li class="dropdown">
        <button class="dropdown-toggle button" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" id="btnGestionUsuarios">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>  <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> Gestion de Tareas <span class="caret"></span> 
        </button>
        <ul class="dropdown-menu">
          <li>
          <form action="CrearProyectoAdmin.htm" method="post" id="menuPrincipalAdminCrearProyecto">
            <button class="button" type="submit" id="btnDarAltas">
            <span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>   <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>
              Crear Proyecto
            </button>
           </form>
          </li>
          <li>
          <form action="CrearTareaAdmin.htm" method="post" id="menuPrincipalAdminCrearTarea">
            <button class="button" type="submit" id="btnDarBajas">
            <span class="glyphicon glyphicon-file" aria-hidden="true"></span> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>
              Crear Tarea
            </button>
           </form>
          </li>
          <li role="separator" class="divider"></li>
          <li>
          <form action="VerTareasAdmin.htm" method="post" id="menuPrincipalAdminVerTareas">
            <button class="button" type="submit" id="btnCambiarRoles">
              <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>  <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>
              Ver Tareas propias
            </button>
           </form>
          </li>
          <li role="separator" class="divider"></li>
        </ul>
      </li>
      
      <li>
        <form action="VerUsuarios.htm" method="post" id="menuPrincipalAdminVerUsuarios">
        <button class="button" type="submit" id="btnListadoUsuarios">
        <span class="glyphicon glyphicon-user" aria-hidden="true"></span> <span class="glyphicon glyphicon-list" aria-hidden="true"></span> Listado de Usuarios</button>
        </form>
        </li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
      <li class="dropdown">
        <button class="dropdown-toggle button" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" id="btnOpciones">
        <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Opciones <span class="caret"></span> 
        </button>
        <ul class="dropdown-menu">
          <li>
          <form action="VerPerfilAdmin.htm" method="post" id="menuPrincipalAdminVerPerfilAdmin">
            <button class="button" type="submit" id="btnVerPerfil">
            <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
              Ver perfil
            </button>
            </form>
          </li>
          <li role="separator" class="divider"></li>
          <li>
          <form action="CerrarSesion.htm" method="post" id="menuPrincipalAdminCerrarSesion">
            <button class="button" type="submit" id="btnCerrarSesion">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              Cerrar Sesion
            </button>
           </form>
          </li>
          <li role="separator" class="divider"></li>
        </ul>
      </li>
    </ul>
    </div>
  </div>
</nav>
<div class="page-header">
	<h2>
		Vista principal Administrador
	</h2>
</div>
	
</body>
</html>