<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
  <!� Latest compiled and minified CSS �>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!� Optional theme �>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../css/estilos.css" />
  <link href="https://mottie.github.io/tablesorter/docs/css/prettify.css" rel="stylesheet">
  <link href="https://mottie.github.io/tablesorter/css/theme.grey.css" rel="stylesheet">
  
  <!� Latest compiled and minified JavaScript �>
  <script src="http://code.jquery.com/jquery-latest.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript" src="https://mottie.github.io/tablesorter/js/jquery.tablesorter.js"></script>
  <script type="text/javascript" src="https://mottie.github.io/tablesorter/js/jquery.tablesorter.widgets.js"></script> 

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">
	<title> Crear Tarea</title>
</head>
 <style type="text/css">
    .button_active {
        background:black;
        cursor:pointer;
        border:none;
        height: 50px;
        position: relative;
        color: grey;
    }
    .button {
        background:none;
        cursor:pointer;
        border:none;
        height: 50px;
        position: relative;
        color: grey;
    }
    .button:hover {color: #FFFFFF}
    .button_active:hover {color: #FFFFFF}
    
</style>
  <body>

  	<div class="row">
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">TodoApp</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
    
      <ul class="nav navbar-nav">
        <li class="active">
        <form action="HomeUser.htm" method="post" id="menuPrincipalNewTask">
        <button class="button" type="submit" id="btnHomeAdmin">
          <span class="glyphicon glyphicon-home" aria-hidden="true" disabled></span>
            Principal  
          </button>
         </form>
        </li>
        
        <li class="active">
        <form action="NewProjectUser.htm" method="post" id="NewProjectNewTask">
        <button class="button" type="submit" id="btnNewProjectUser">
          <span class="glyphicon glyphicon-folder-close" aria-hidden="true" disabled></span>
          <span class="glyphicon glyphicon-ok" aria-hidden="true" disabled></span>
            Crear Proyecto  
          </button>
         </form>
        </li>
        
        <li class="active">
        <form action="ChangePasswordUser.htm" method="post" id="ChangePasswordNewTask">
        <button class="button" type="submit" id="btnChangePasswordUser">
          <span class="glyphicon glyphicon-font" aria-hidden="true" disabled></span>
          <span class="glyphicon glyphicon-pencil" aria-hidden="true" disabled></span>
            Cambiar Contrase�a  
          </button>
         </form>
        </li>        
      
      <ul class="nav navbar-nav navbar-right">
      <li class="dropdown">
        <button class="dropdown-toggle button" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" id="btnOpciones">
        <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Opciones <span class="caret"></span> 
        </button>
        <ul class="dropdown-menu">
          <li>
          <form action="VerPerfilUsuario.htm" method="post" id="perfilNewTask">
            <button class="button" type="submit" id="btnVerPerfil">
            <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
              Ver perfil
            </button>
            </form>
          </li>
          <li role="separator" class="divider"></li>
          <li>
          <form action="CerrarSesion.htm" method="post" id="CloseNewTask">
            <button class="button" type="submit" id="btnCerrarSesion">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              Cerrar Sesion
            </button>
           </form>
          </li>
          <li role="separator" class="divider"></li>
        </ul>
      </li>
    </ul>
    </div>
  </div>
</nav>
</div>
<form action="InfoTaskUser.htm" method="post" id="ChangePasswordUser">

    <div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<form class="form-horizontal" role="form">
				<div class="form-group">
					 
					<label for="inputNombre" class="col-sm-1 control-label">
						Nombre
					</label>
					<div class="col-sm-8">
						<input class="form-control" id="inputNombre" type="email" disabled="true">
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4" id="alineartarea">
			<form class="form-horizontal" role="form">
				
					<label for="inputPrioridad" class="control-label" disabled="">
								Prioridad
					</label>
					<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						Prioridad
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
						<li class = "disabled"><a href="#">Alta</a></li>
						<li class = "disabled"><a href="#">Media</a></li>
						<li class = "disabled"><a href="#">Baja</a></li>
					</ul>
				
			</form>
		</div>
		<div class="col-sm-4" id="alineartarea">
			<form class="form-horizontal" role="form">
					<label for="inputPrioridad" class="control-label">
								Proyecto
					</label>
					<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						Proyecto
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
						<li class="disabled"><a href="#">Proyecto 1</a></li>
						<li class="disabled"><a href="#">Proyecto 2</a></li>
						<li class="disabled"><a href="#">Proyecto 3</a></li>
					</ul>
			</form>
		</div>
		<div class="col-sm-4" id="alineartarea">
			<input type="radio" name="completada" value="completada" disabled="true" unchecked> Completada<br>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4" id="alineartarea">
			<p>Fecha Inicio: <input type="text" id="datepicker1" disabled="true"></p>
			<script>
			  $( function() {
				$( "#datepicker1" ).datepicker();
			  } );
			</script>
		</div>
		<div class="col-sm-8" id="alineartarea">
			<p>Fecha Limite: <input type="text" id="datepicker2" disabled="true"></p>
			<script>
			  $( function() {
				$( "#datepicker2" ).datepicker();
			  } );
			</script>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<form class="form-horizontal" role="form">
				<div class="form-group">
					 
					<label for="inputNotas" class="col-sm-1 control-label">
						Notas
					</label>
					<div class="col-sm-8">
						<input class="form-control" id="inputNotas" type="email" disabled="true">
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
				
	<button type="button" class="btn btn-default btn-sm">
  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Modificar
</button>
			</div>
		</div>
	</div>
</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
</html>