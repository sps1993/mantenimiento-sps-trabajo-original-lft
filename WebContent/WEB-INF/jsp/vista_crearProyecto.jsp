<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<!DOCTYPE html>
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
  <!— Latest compiled and minified CSS —>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!— Optional theme —>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../css/estilos.css" />
  <link href="https://mottie.github.io/tablesorter/docs/css/prettify.css" rel="stylesheet">
  <link href="https://mottie.github.io/tablesorter/css/theme.grey.css" rel="stylesheet">
  
  <!— Latest compiled and minified JavaScript —>
  <script src="http://code.jquery.com/jquery-latest.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript" src="https://mottie.github.io/tablesorter/js/jquery.tablesorter.js"></script>
  <script type="text/javascript" src="https://mottie.github.io/tablesorter/js/jquery.tablesorter.widgets.js"></script> 

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Crear Proyecto</title>

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">
    
    <script>
		window.setTimeout(function() {
		    $(".alert").fadeTo(500, 0).slideUp(500, function(){
		        $(this).remove(); 
		    });
		}, 4000);
	</script>
</head>
 <style type="text/css">
    .button_active {
        background:black;
        cursor:pointer;
        border:none;
        height: 50px;
        position: relative;
        color: grey;
    }
    .button {
        background:none;
        cursor:pointer;
        border:none;
        height: 50px;
        position: relative;
        color: grey;
    }
    .button:hover {color: #FFFFFF}
    .button_active:hover {color: #FFFFFF}
    
</style>
  <body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">TodoApp</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
    
      <ul class="nav navbar-nav">
        <li class="active">
        <form action="HomeUser.htm" method="post" id="menuPrincipalUserHome">
        <button class="button" type="submit" id="btnHomeAdmin">
          <span class="glyphicon glyphicon-home" aria-hidden="true" disabled></span>
            Principal  
          <span class="sr-only"> (current)</span>
          </button>
         </form>
        </li>
        
        <li class="active">
        <form action="NewProjectUser.htm" method="post" id="NewProjectUserHome">
        <button class="button_active" type="submit" id="btnNewProjectUser" disabled>
          <span class="glyphicon glyphicon-folder-close" aria-hidden="true" disabled></span>
          <span class="glyphicon glyphicon-ok" aria-hidden="true" disabled></span>
            Crear Proyecto  
          </button>
         </form>
        </li>
        
        <li class="active">
        <form action="ChangePasswordUser.htm" method="post" id="ChangePasswordUserHome">
        <button class="button" type="submit" id="btnChangePasswordUser">
          <span class="glyphicon glyphicon-font" aria-hidden="true" disabled></span>
          <span class="glyphicon glyphicon-pencil" aria-hidden="true" disabled></span>
            Cambiar Contraseña  
          </button>
         </form>
        </li> 
        
        </ul>       
      
      <ul class="nav navbar-nav navbar-right">
      <li class="dropdown">
        <button class="dropdown-toggle button" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" id="btnOpciones">
        <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Opciones <span class="caret"></span> 
        </button>
        <ul class="dropdown-menu">
          <li>
          <form action="VerPerfilUsuario.htm" method="post" id="menuPrincipalAdminVerPerfilAdmin">
            <button class="button" type="submit" id="btnVerPerfil">
            <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
              Ver perfil
            </button>
            </form>
          </li>
          <li role="separator" class="divider"></li>
          <li>
          <form action="CerrarSesion.htm" method="post" id="menuPrincipalAdminCerrarSesion">
            <button class="button" type="submit" id="btnCerrarSesion">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              Cerrar Sesion
            </button>
           </form>
          </li>
          <li role="separator" class="divider"></li>
        </ul>
      </li>
    </ul>
    </div>
  </div>
</nav>


<form action="crearNuevoProyecto.htm" method="post" id="ChangePasswordUser">

	<div class="row">
		<div class="col-sm-11" id="headerwrap">
			<h3 class="text-center" id="header">
				Crear Proyecto
			</h3>
		</div>
	</div>
		
		<div class="col-sm-11">
				<div class="form-group">
					 
					<label for="inputProject" class="control-label">
						Nombre Proyecto
					</label>
					</div>
					<div class="col-sm-8">
						<input class="form-control" id="inputEmail3" type="text" name="nuevoProyecto">
					</div>
				</div>

			<br/>
		<div class="col-sm-11">
			
				<div class="form-group">
					 
					<label for="inputProp" class="control-label">
						Propietarios
					</label>
					</div>
					<div class="col-sm-8">
						<input class="form-control" id="inputNombre" type="text" placeholder="correo1@gmail.com, correo2@gmail.com..." name="propietariosProyecto">
					</div>
				
						
		<br/><br/>
		<div class="col-sm-6">
				
			<button type="submit" class="btn btn-default btn-sm">
  				<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
			</button>
	
		</div>
	</div>
</form>
<br/><br/>
		<div class="row">
		<div class="col-sm-6">
			<c:set var="comienzoMensaje" value="${comienzoMensaje}"/>
			<c:set var="Mensaje" value="${Mensaje}"/>
			<c:if test="${(!empty comienzoMensaje) && (!empty Mensaje)}">
					<div class="alert alert-success alert-dismissible" id="MensajeCorrecto" fade in>
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<p>
							<b>${comienzoMensaje}</b>
							${Mensaje}
						</p>
					</div>
			</c:if>
			<c:set var="comienzoMensaje" value="${comienzoMensajeError}"/>
			<c:set var="Mensaje" value="${MensajeError}"/>
			<c:if test="${(!empty comienzoMensajeError) && (!empty MensajeError)}">
					<div class="alert alert-danger alert-dismissible" id="Mensajeincorrecto" fade in>
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<p>
							<b>${comienzoMensajeError}</b>
							${MensajeError}
						</p>
					</div>
			</c:if>
		</div>
	</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
</html>