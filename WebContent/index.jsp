<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>
    
    <!â Latest compiled and minified CSS â>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!â Optional theme â>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../css/estilos.css" />
  <link href="https://mottie.github.io/tablesorter/docs/css/prettify.css" rel="stylesheet">
  <link href="https://mottie.github.io/tablesorter/css/theme.grey.css" rel="stylesheet">
  
  <!â Latest compiled and minified JavaScript â>
  <script src="http://code.jquery.com/jquery-latest.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript" src="https://mottie.github.io/tablesorter/js/jquery.tablesorter.js"></script>
  <script type="text/javascript" src="https://mottie.github.io/tablesorter/js/jquery.tablesorter.widgets.js"></script> 

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">

	<title>Pagina de Inicio</title>
	
	<script>
		window.setTimeout(function() {
		    $(".alert").fadeTo(500, 0).slideUp(500, function(){
		        $(this).remove(); 
		    });
		}, 4000);
	</script>
	
</head>
<body>

	<div style="text-align: center;">
		<h2>
			TodoApp
		</h2>
	</div>
	<br/>
	<form action="entrar.htm" method="post">
		<div class="row">
			<div class="col-sm-4" style="text-align: right;">Introduzca su nombre: </div>
			<div class="col-sm-4">
				<input class="form-control" name="userEmailLogin" type="email" id="userEmailLogin">
			</div>
			<div class="col-sm-4"></div>
		</div>
		
		<br/>
		<div class="row">
			<div class="col-sm-4" style="text-align: right;">Introduzca su contraseña: </div>
			<div class="col-sm-4">
				<input class="form-control" name="contrasenaLogin" type="password" id="contrasenaLogin">
			</div>
			<div class="col-sm-4"></div>
		</div>	
		<br/>
		
		<div class="row">
			<div class="col-sm-4"></div>
			<div class="col-sm-4"><p><a href="http://localhost:8080/Sprint_1/InterfazRecuperarContrasena.htm" id="RecuperarContrasena">Recuperar clave</a></p> </div>
			<div class="col-sm-4"></div>
		</div>
		
			<br /> <br />
			<div class="col-sm-6" style="text-align: right;">
				<button type="submit" class="btn btn-default" id="botonEntrar"> Entrar </button>
			</div>

	</form>
	<form action="registrar.htm" method="post">
		<div class="col-sm-6" style="text-align: left;">
			<button type="submit" class="btn btn-default" id="botonRegistrar"> Registrar </button>
		</div>
	</form>
 <br/><br/><br/><br/>
	<div class="row">
		<div class="col-sm-6">
			<c:set var="comienzoMensaje" value="${comienzoMensaje}"/>
			<c:set var="Mensaje" value="${Mensaje}"/>
			<c:if test="${(!empty comienzoMensaje) && (!empty Mensaje)}">
					<div class="alert alert-success alert-dismissible" id="MensajeCorrecto" fade in>
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<p>
							<b>${comienzoMensaje}</b>
							${Mensaje}
						</p>
					</div>
			</c:if>
			<c:set var="comienzoMensaje" value="${comienzoMensajeError}"/>
			<c:set var="Mensaje" value="${MensajeError}"/>
			<c:if test="${(!empty comienzoMensajeError) && (!empty MensajeError)}">
					<div class="alert alert-danger alert-dismissible" id="Mensajeincorrecto" fade in>
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<p>
							<b>${comienzoMensajeError}</b>
							${MensajeError}
						</p>
					</div>
			</c:if>
		</div>
	</div>
</body>
</html>