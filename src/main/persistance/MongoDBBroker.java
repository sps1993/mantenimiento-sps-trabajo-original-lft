package main.persistance;

import java.net.UnknownHostException;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class MongoDBBroker {

	private String dbName;
	private static MongoDBBroker _instance;
	private MongoClientURI clienteURI;
	private MongoClient cliente;
	private DB database;
	
	private MongoDBBroker() throws UnknownHostException {
		this.dbName = "mongodb://usuario:usuario@ds145208.mlab.com:45208/mibbdd-todoapp-mant";
		this.clienteURI  = new MongoClientURI(this.dbName); 
        this.cliente = new MongoClient(this.clienteURI);
        this.database=cliente.getDB(clienteURI.getDatabase());
 
	}

	public static MongoDBBroker getInstance() throws UnknownHostException {
		if (_instance == null){
			_instance = new MongoDBBroker();
		}
		return _instance;
	}
	
	public DBCollection getCollection(String tabla){
		return this.database.getCollection(tabla);
	}
}
