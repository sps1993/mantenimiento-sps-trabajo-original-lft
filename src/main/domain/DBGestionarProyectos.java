package main.domain;

import java.util.ArrayList;
import java.util.LinkedList;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import main.persistance.DataBase;

public class DBGestionarProyectos {
	
	DataBase mInstance;
	Email e = new Email();
	
	public DBGestionarProyectos(){
		mInstance=DataBase.getInstance();
	}
	
	public boolean crearProyecto(LinkedList<String>email, String nProyecto){
		boolean enviado=false;
		DBObject document=new BasicDBObject();
		String propietarios="";
		LinkedList<String> propietariosProyecto = new LinkedList<String>();
		for(int i=0;i<email.size();i++){
			propietarios+=email.get(i)+", ";
		}
		
		propietarios=propietarios.substring(0, propietarios.length()-2);
		if(email.size()==1){
			document.put("email", propietarios);
			document.put("nProyecto", nProyecto);
			enviado=e.enviarEmail(email.get(0), "[TodoApp] Crear Proyecto {Simple}", "El proyecto " + nProyecto + " se ha creado con �xito.");
		}
		else{
			String[] prop = propietarios.split(", ");
			document.put("email", propietarios);
			document.put("nProyecto", nProyecto);
			for(int i=0; i<prop.length; i++){
				//propietariosProyecto.add(prop[i]);
				enviado=e.enviarEmail(prop[i], "[TodoApp] Crear Proyecto {Compartido}", "El proyecto " + nProyecto + " se ha creado con �xito.");
			}
		}
		
		if(enviado){
			mInstance.createProyecto(document);
		}
		return enviado;
	}
	
	public int nProyectos(LinkedList<String>email){
		DBObject document=new BasicDBObject();
		LinkedList<DBObject>proyectos=mInstance.readProyecto(document);
		int i=0, nI=0;
		for(int j=0; j<proyectos.size(); j++){
			BasicDBList users=(BasicDBList)proyectos.get(j).get("nProyecto");
			for(int k=0; k<email.size();k++){
				for(int l=0;l<users.size();l++){
					if(email.get(k).equalsIgnoreCase(users.get(l).toString())){
						nI++;
					}
				}
			}
			if(nI==email.size()){
				i++;
			}
			nI=0;
		}
		return i;
	}
	
	public boolean borrarProyecto(LinkedList<String>email, String nombre){
		DBObject document=new BasicDBObject(), documentNuevo=new BasicDBObject();
		boolean borrado=false, masDeUno=false;
		LinkedList<String>lp=new LinkedList<String>();
		LinkedList<String>lpMasDeUno=new LinkedList<String>();
		LinkedList<DBObject>proyectos=mInstance.readProyecto(document);
		ArrayList<String>emailTotalProyectos=new ArrayList<String>();
		String componentesProyecto="";
		LinkedList<String>listaComponentesProyectos = new LinkedList<String>();
		for(int j=0; j<proyectos.size(); j++){
			emailTotalProyectos.add(proyectos.get(j).get("email").toString());
		}
		
			for(int i=0; i<email.size();i++){
				for(int k=0; k<emailTotalProyectos.size();k++){
					if((emailTotalProyectos.get(k)).contains(email.get(i))){
						if((emailTotalProyectos.get(k)).equalsIgnoreCase(email.get(i))){
							masDeUno=false;
							lp.add(proyectos.get(k).get("nProyecto").toString());
						}
						else{
							masDeUno=true;
							lpMasDeUno.add(proyectos.get(k).get("nProyecto").toString());
							componentesProyecto=proyectos.get(k).get("email").toString();
							listaComponentesProyectos.add(componentesProyecto);
						}
						
					}
				}
			}
		if(masDeUno){
			String nombreActualizar = "";
			for(int i=0;i<lpMasDeUno.size();i++){
				String nombres=listaComponentesProyectos.get(i);
				String[] nombresPorSeparado=nombres.split(", ");
				
				for(int h=0;h<nombresPorSeparado.length;h++){
					if( nombresPorSeparado[h].equals(email.get(0)) );
					else nombreActualizar+=nombresPorSeparado[h]+", ";
				}
				nombreActualizar=nombreActualizar.substring(0, nombreActualizar.length()-2);
				if(lpMasDeUno.get(i).equalsIgnoreCase(nombre)){
					documentNuevo.put("email", nombreActualizar);
					documentNuevo.put("nProyecto", nombre);
					mInstance.updateProyecto(document,documentNuevo);
				}
			}
		}
		else{
			for(int i=0;i<lp.size();i++){
				if(lp.get(i).equals(nombre)){
					document.put("nProyecto", lp.get(i));
					mInstance.deleteProyecto(document);
				}
			}
		}
		
		return borrado;
		
	}
	
	public boolean borrarProyectos(LinkedList<String>email){
		DBObject document=new BasicDBObject(), documentNuevo=new BasicDBObject(), documentCompartido=new BasicDBObject();
		boolean borrado=false, masDeUno=false, igualAUno=false;
		LinkedList<String>lp=new LinkedList<String>();
		LinkedList<String>lpMasDeUno=new LinkedList<String>();
		LinkedList<DBObject>proyectos=mInstance.readProyecto(document);
		ArrayList<String>emailTotalProyectos=new ArrayList<String>();
		String componentesProyecto="";
		LinkedList<String>listaComponentesProyectos = new LinkedList<String>();
		for(int j=0; j<proyectos.size(); j++){
			emailTotalProyectos.add(proyectos.get(j).get("email").toString());
		}
		
			for(int i=0; i<email.size();i++){
				for(int k=0; k<emailTotalProyectos.size();k++){
					if((emailTotalProyectos.get(k)).contains(email.get(i))){
						if((emailTotalProyectos.get(k)).equalsIgnoreCase(email.get(i))){
							igualAUno=true;
							lp.add(proyectos.get(k).get("nProyecto").toString());
						}
						else{
							masDeUno=true;
							lpMasDeUno.add(proyectos.get(k).get("nProyecto").toString());
							componentesProyecto=proyectos.get(k).get("email").toString();
							listaComponentesProyectos.add(componentesProyecto);
						}
						
					}
				}
				if(masDeUno){
					String nombreActualizar = "";
					for(int k=0;k<lpMasDeUno.size();k++){
						String nombres=listaComponentesProyectos.get(k);
						
						String[] nombresPorSeparado=nombres.split(", ");
						
						for(int h=0;h<nombresPorSeparado.length;h++){
							if( nombresPorSeparado[h].equals(email.get(0)) );
							else nombreActualizar+=nombresPorSeparado[h]+", ";
						}
						nombreActualizar=nombreActualizar.substring(0, nombreActualizar.length()-2);
						
						documentNuevo.put("email", nombreActualizar);
						documentNuevo.put("nProyecto", lpMasDeUno.get(k));
						
						documentCompartido.put("email", nombres);
						documentCompartido.put("nProyecto", lpMasDeUno.get(k));
						
						System.out.println("Antes de entrar en el updateProyecto:" + nombreActualizar + "nProyecto: " + lpMasDeUno.get(k));
						mInstance.updateProyecto(documentCompartido,documentNuevo);
					}
				}
				if(igualAUno){
					for(int p=0;p<lp.size();p++){
						document.put("nProyecto", lp.get(p));
						mInstance.deleteProyecto(document);
					}
				}
			}
		
		
		return borrado;
		
	}
	
	public LinkedList<String> leerProyectos(LinkedList<String>email){
		DBObject document=new BasicDBObject();
		LinkedList<String>lp=new LinkedList<String>();
		LinkedList<DBObject>proyectos=mInstance.readProyecto(document);
		ArrayList<String>emailTotalProyectos=new ArrayList<String>();
		
		for(int j=0; j<proyectos.size(); j++){
			emailTotalProyectos.add(proyectos.get(j).get("email").toString());
		}
		
			for(int i=0; i<email.size();i++){
				for(int k=0; k<emailTotalProyectos.size();k++){
					if((emailTotalProyectos.get(k)).contains(email.get(i))){
						lp.add(proyectos.get(k).get("nProyecto").toString());
					}
				}
			}
		return lp;
	}
	
	public boolean existeProyecto(LinkedList<String>email, String nombre){
		DBObject document=new BasicDBObject();
		LinkedList<String>lp=new LinkedList<String>();
		LinkedList<DBObject>proyectos=mInstance.readProyecto(document);
		ArrayList<String>emailTotalProyectos=new ArrayList<String>();
		
		for(int j=0; j<proyectos.size(); j++){
			emailTotalProyectos.add(proyectos.get(j).get("email").toString());
		}
		
			for(int i=0; i<email.size();i++){
				for(int k=0; k<emailTotalProyectos.size();k++){
					if((emailTotalProyectos.get(k)).contains(email.get(i))){
						lp.add(proyectos.get(k).get("nProyecto").toString());
					}
				}
			}
		for(int l=0; l<lp.size(); l++){
			if(lp.get(l).equalsIgnoreCase(nombre)){
				return true;
			}
		}
		return false;
	}
	
	public Proyecto leerProyecto(LinkedList<String>email, String nombre){
		DBObject document=new BasicDBObject();
		LinkedList<String>lp=new LinkedList<String>();
		LinkedList<DBObject>proyectos=mInstance.readProyecto(document);
		ArrayList<String>emailTotalProyectos=new ArrayList<String>();
		
		for(int j=0; j<proyectos.size(); j++){
			emailTotalProyectos.add(proyectos.get(j).get("email").toString());
		}
		
			for(int i=0; i<email.size();i++){
				for(int k=0; k<emailTotalProyectos.size();k++){
					if((emailTotalProyectos.get(k)).contains(email.get(i))){
						lp.add(proyectos.get(k).get("nProyecto").toString());
					}
				}
			}
		for(int l=0; l<lp.size(); l++){
			if(lp.get(l).equalsIgnoreCase(nombre)){
				return new Proyecto(email, nombre);
			}
		}
		return new Proyecto();
	}
	
	public boolean actualizarProyecto(Proyecto pAntiguo, Proyecto pNuevo){
		this.borrarProyecto(pAntiguo.getUsuario(), pAntiguo.getProyecto());
		this.crearProyecto(pNuevo.getUsuario(), pNuevo.getProyecto());
		return true;
	}
	
}