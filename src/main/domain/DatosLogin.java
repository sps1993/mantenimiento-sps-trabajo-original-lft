package main.domain;

public class DatosLogin {
	
	DBGestionarUsuario mInstancia;
	
	public DatosLogin(){
		mInstancia=DBGestionarUsuario.getInstance();
	}
	
	public String getContrasenaLogin(String email){
		return mInstancia.recuperarClave(email);
	}

	public String getEmailLogin(){
		return mInstancia.emailLogin;
	}

	public void setEmailLogin(String email){
		mInstancia.emailLogin=email;
	}
	
}
