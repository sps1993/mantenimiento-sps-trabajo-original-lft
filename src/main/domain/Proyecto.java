package main.domain;

import java.util.LinkedList;

public class Proyecto {
	private LinkedList<String> usuario;
	private String proyecto;
	//private DAOProyecto dao;
	private DBGestionarProyectos dao;

	public Proyecto(LinkedList<String> u, String p) {
		super();
		this.usuario = u;
		this.proyecto = p;
		dao = new DBGestionarProyectos();
	}

	public Proyecto(){
		dao = new DBGestionarProyectos();
	}

	public LinkedList<String> todosProyectos(LinkedList<String> user){
		return dao.leerProyectos(user);
	}
	
	public void buscarP(LinkedList<String> user, String p){
		Proyecto pr = dao.leerProyecto(user, p); /*Devuelve BOOLEAN en vez del Proyecto*/
		this.setUsuario(pr.getUsuario());
		this.setProyecto(pr.getProyecto());
	}
	
	public boolean read(){
		return dao.existeProyecto(this.getUsuario(), this.getProyecto());
	}

	public LinkedList<String> readAll(){
		return dao.leerProyectos(this.getUsuario());
	}
	
	public void update(Proyecto pNuevo){
		dao.actualizarProyecto(this, pNuevo);
		this.setProyecto(pNuevo.getProyecto());
		this.setUsuario(pNuevo.getUsuario());
	}

	public void create(){
		dao.crearProyecto(this.getUsuario(), this.getProyecto());
	}

	public void delete(){
		dao.borrarProyecto(this.getUsuario(), this.getProyecto());
	}
	
	public void deleteAll(){
		dao.borrarProyectos(this.getUsuario());
	}
	
	public int nProyectos(){
		return dao.nProyectos(this.getUsuario());
	}

	public LinkedList<String> getUsuario() {
		return this.usuario;
	}

	public boolean comprobarUsuarios(LinkedList<String>usuarios, String proy){
		return dao.existeProyecto(this.getUsuario(), this.getProyecto());
	}
	
	public void setUsuario(LinkedList<String> u) {
		this.usuario = u;
	}

	public String getProyecto() {
		return this.proyecto;
	}

	public void setProyecto(String p) {
		this.proyecto = p;
	}
	
	public DBGestionarProyectos getDAO(){
		return this.dao;
	}
	
	public boolean equals(Proyecto p){
		return p.getUsuario().equals(this.getUsuario()) && p.getProyecto().equals(this.proyecto);
	}
}
