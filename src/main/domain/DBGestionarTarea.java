package main.domain;

import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import main.persistance.DataBase;
import main.persistance.MongoDBBroker;

public class DBGestionarTarea {
	static DBGestionarTarea mInstancia;
	DataBase mInstance;
	DBObject document;
	DBCursor dc;
	String tareaInfo;
	Email e = new Email();
	
	public DBGestionarTarea(){
		mInstance=DataBase.getInstance();
	}
	
	public static DBGestionarTarea getInstance(){
		if(mInstancia==null){
			mInstancia=new DBGestionarTarea();
		}
		return mInstancia;
	}
	
	public void avisos(String user){
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		Calendar c = Calendar.getInstance();
		String fO=(formato.format(c.getTime())).split(" ")[0];
		int aA=Integer.parseInt(fO.split("-")[0]);
		int mA=Integer.parseInt(fO.split("-")[1]);
		int dA=Integer.parseInt(fO.split("-")[2]);
		c.setTime(new Date());
		c.add(Calendar.DAY_OF_YEAR, 31);
		String fecha=(formato.format(c.getTime())).split(" ")[0];
		int aF=Integer.parseInt(fecha.split("-")[0]);
		int mF=Integer.parseInt(fecha.split("-")[1]);
		int dF=Integer.parseInt(fecha.split("-")[2]);
		String avisos="", avisos2="";
		DBCollection coll;
		try {
			coll = MongoDBBroker.getInstance().getCollection("Tareas");
			BasicDBObject doc = new BasicDBObject();
			doc.append("usuario",user);
			DBCursor cursor = coll.find(doc);
			DBObject tmpobj;
			String nombre, fecha_fin;
			while(cursor.hasNext()) {
				tmpobj = cursor.next();
				nombre = tmpobj.get("nombre").toString(); 
				fecha_fin = tmpobj.get("fecha").toString();
				int dT=Integer.parseInt(fecha_fin.split("/")[0]);
				int mT=Integer.parseInt(fecha_fin.split("/")[0]);
				int aT=Integer.parseInt(fecha_fin.split("/")[0]);
				if(dT>=dA && dT<=dF && mT>=mA && mT<=mF && aT>=aA && aT<=aF){
					avisos=avisos+"\n"+nombre+" - "+tmpobj.get("proyecto").toString()+" - "+fecha_fin;
				}else{
					if(mT>=mA && mT<=mF && aT>=aA && aT<=aF){
						avisos=avisos+"\n"+nombre+" - "+tmpobj.get("proyecto").toString()+" - "+fecha_fin;
					}else{
						if(aT>=aA && aT<=aF){
							avisos=avisos+"\n"+nombre+" - "+tmpobj.get("proyecto").toString()+" - "+fecha_fin;
						}
					}
				}
				if(dT<dA && mT<=mA && aT<=aA){
					avisos2=avisos2+"\n"+nombre+" - "+tmpobj.get("proyecto").toString()+" - "+fecha_fin;
				}else{
					if(mT<mA && aT<=aA ){
						avisos2=avisos2+"\n"+nombre+" - "+tmpobj.get("proyecto").toString()+" - "+fecha_fin;
					}else{
						if(aT<aA){
							avisos2=avisos2+"\n"+nombre+" - "+tmpobj.get("proyecto").toString()+" - "+fecha_fin;
						}
					}
				}
			}
			if(!(avisos.equalsIgnoreCase(""))){
				Email  e=new Email();
				e.enviarEmail(user, "[TodoApp] Proximamente, estas tareas caducaran.", "Estas son las tareas que estan proximos a caducar:"+avisos);
			}
			if(!(avisos2.equalsIgnoreCase(""))){
				Email  e=new Email();
				e.enviarEmail(user, "[TodoApp] Tareas Caducadas.", "Estas son las tareas que han caducado: "+avisos2);
			}
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}
		
	}
	
	public boolean createTarea(String email, String nTarea, String prioridad, String proyecto, boolean completada, String fechaInicio, String fechaFin, String notas){
		boolean enviado=false;
		document=new BasicDBObject();
		document.put("email", email);
		document.put("nombreTarea", nTarea);
		document.put("prioridad", prioridad);
		document.put("proyecto", proyecto);
		document.put("completada", completada);
		document.put("fechaInicio", fechaInicio);
		document.put("fechaFin", fechaFin);
		document.put("notas",  notas);
		enviado=e.enviarEmail(email, "[TodoApp] Nueva Tarea", "Se ha creado la siguiente tarea: "+ nTarea);
		if(enviado) mInstance.createTarea(document);
		return enviado;
	}
	
	public boolean createTareaUpdate(String email, String nTarea, String prioridad, String proyecto, boolean completada, String fechaInicio, String fechaFin, String notas){
		boolean enviado=false;
		document=new BasicDBObject();
		document.put("email", email);
		document.put("nombreTarea", nTarea);
		document.put("prioridad", prioridad);
		document.put("proyecto", proyecto);
		document.put("completada", completada);
		document.put("fechaInicio", fechaInicio);
		document.put("fechaFin", fechaFin);
		document.put("notas",  notas);
		enviado=e.enviarEmail(email, "[TodoApp] Actualizar Tarea", "Se ha modificado la siguiente tarea: "+ nTarea);
		if(enviado) mInstance.createTarea(document);
		return enviado;
	}
	
	public boolean modificar(Tarea tV, Tarea tN){
		this.deleteTarea(tV.getEmail(), tV.getnTarea());
		this.createTarea(tN.getEmail(), tN.getnTarea(), tN.getPrioridad(), tN.getProyecto(), tN.isCompletada(), tN.getFechaInicio(), tN.getFechaFin(), tN.getNotas());
		return this.existeTarea(tN.getEmail(), tN.getnTarea());
	}
	
	public int nTareas(String email){
		DBObject document=new BasicDBObject();
		document.put("email", email);
		return mInstance.readTarea(document).size();
	}
	
	public void borrarTareas(String email){
		DBObject document=new BasicDBObject();
		document.put("email", email);
		mInstance.deleteTarea(document);
	}
	
	public boolean updateTarea(String email, String nTarea, String prioridad, String proyecto, boolean completada, String fechaInicio, String fechaFin, String notas){
		boolean enviado=false;
		document=new BasicDBObject();
		document.put("email", email);
		document.put("nombreTarea", nTarea);
		dc=mInstance.readTarea(document);
		if(dc.count()==1){
			DBObject antiguo=dc.next();
			document=new BasicDBObject();
			document.put("email", email);
			document.put("nombreTarea", nTarea);
			document.put("prioridad", prioridad);
			document.put("proyecto", proyecto);
			document.put("completada", completada);
			document.put("fechaInicio", fechaInicio);
			document.put("fechaFin", fechaFin);
			document.put("notas",  notas);
			enviado=e.enviarEmail(email, "[TodoApp] Actualizar Tarea", "Se ha modificado la siguiente tarea: "+ nTarea);
			if(enviado) mInstance.updateTarea(antiguo, document);
			return enviado;
		}else return enviado;
	}

	public LinkedList<Tarea> readTareas(){
		DBCursor users=mInstance.readAllTareas();
		LinkedList<Tarea> tareas=new LinkedList<Tarea>();
		DBObject t;
		for (int i=0;i<users.size();i++){
			t=users.next();
			Tarea tareaAñadida = new Tarea(t.get("email").toString(), t.get("nombreTarea").toString(), t.get("prioridad").toString(), t.get("proyecto").toString(), t.get("fechaInicio").toString(), t.get("fechaFin").toString(), t.get("notas").toString(), (boolean)t.get("completada"));
			tareas.add(tareaAñadida);
		}
		return tareas;
	}
	
	public LinkedList<Tarea> readTareasOrdenadasPorFecha(){
		DBCursor users=mInstance.readTareaOrdenadaPorFecha();
		LinkedList<Tarea> tareas=new LinkedList<Tarea>();
		DBObject t;
		while(users.hasNext()){
			t=users.next();
			tareas.add(new Tarea(t.get("email").toString(), t.get("nombreTarea").toString(), t.get("prioridad").toString(), t.get("proyecto").toString(), t.get("fechaInicio").toString(), t.get("fechaFin").toString(), t.get("notas").toString(), (boolean)t.get("completada")));
		}
		return tareas;
	}
	
	public LinkedList<Tarea> readTareasOrdenadasPorProyecto(){
		DBCursor users=mInstance.readTareaOrdenadaPorProyecto();
		LinkedList<Tarea> tareas=new LinkedList<Tarea>();
		DBObject t;
		while(users.hasNext()){
			t=users.next();
			tareas.add(new Tarea(t.get("email").toString(), t.get("nombreTarea").toString(), t.get("prioridad").toString(), t.get("proyecto").toString(), t.get("fechaInicio").toString(), t.get("fechaFin").toString(), t.get("notas").toString(), (boolean)t.get("completada")));
		}
		return tareas;
	}
	
	public LinkedList<Tarea> readTareasOrdenadasPorPrioridad(){
		DBCursor users=mInstance.readTareaOrdenadaPorPrioridad();
		LinkedList<Tarea> tareas=new LinkedList<Tarea>();
		DBObject t;
		while(users.hasNext()){
			t=users.next();
			tareas.add(new Tarea(t.get("email").toString(), t.get("nombreTarea").toString(), t.get("prioridad").toString(), t.get("proyecto").toString(), t.get("fechaInicio").toString(), t.get("fechaFin").toString(), t.get("notas").toString(), (boolean)t.get("completada")));
		}
		return tareas;
	}
	
	public Tarea readTarea(String email, String nombreTarea){
		Tarea t;
		document=new BasicDBObject();
		document.put("email", email);
		document.put("nombreTarea", nombreTarea);
		DBCursor tarea=mInstance.readTarea(document);
		DBObject ta=tarea.next();
		t=new Tarea(ta.get("email").toString(), ta.get("nombreTarea").toString(), ta.get("prioridad").toString(), ta.get("proyecto").toString(), ta.get("fechaInicio").toString(), ta.get("fechaFin").toString(), ta.get("notas").toString(), (boolean)ta.get("completada"));
		return t;
	}
	
	public boolean existeTarea(String email, String nombreTarea){
		document=new BasicDBObject();
		document.put("email", email);
		document.put("nombreTarea", nombreTarea);
		DBCursor tarea=mInstance.readTarea(document);
		if(tarea.hasNext())return true;
		return false;
	}
	
	public void deleteTarea(String email, String nombreTarea){
		boolean enviado=false;
		document=new BasicDBObject();
		document.put("email", email);
		document.put("nombreTarea", nombreTarea);
		enviado=e.enviarEmail(email, "[TodoApp] Borrar Tarea", "Se ha eliminado la siguiente tarea: "+ nombreTarea);
		if(enviado) mInstance.deleteTarea(document);
	}
	
	public void deleteTareaUpdate(String email, String nombreTarea){
		document=new BasicDBObject();
		document.put("email", email);
		document.put("nombreTarea", nombreTarea);
		mInstance.deleteTarea(document);
	}

	public void desconectar(){
		mInstance.cerrarBD();
	}
	
public LinkedList<String[]> crearTablaTareasCompletadasUser(LinkedList<Tarea> tasks, String email) {
				
		
		LinkedList<String[]> listaTareas =new LinkedList<String[]>();
		
		
		while(!tasks.isEmpty()){
			
			if(tasks.peek().getEmail().equals(email) && tasks.peek().isCompletada()){
				String[] tarea = new String[4];
				tarea[0] = tasks.peek().getnTarea();
				tarea[1] = tasks.peek().getFechaFin();
				tarea[2] = tasks.peek().getProyecto();
				tarea[3] = tasks.peek().getPrioridad();
				listaTareas.add(tarea);
			}
			tasks.remove();
		}
		
		
		return listaTareas;
	}

public LinkedList<String[]> crearTablaTareasActualesUser(LinkedList<Tarea> tasks, String email) {
	
	
	LinkedList<String[]> listaTareas =new LinkedList<String[]>();
	
	Date fechaactual = new Date();
    SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy/MM/dd");
	
	
	while(!tasks.isEmpty()){
		
		Date fechainicio;
		boolean correcto=false;
		try {
			fechainicio = dmyFormat.parse(tasks.peek().getFechaInicio());
			
			if(fechaactual.after(fechainicio)){ //se comprueba que la fecha actual es anterior a la fecha de inicio
				correcto=true;
			}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(tasks.peek().getEmail().equals(email) && !tasks.peek().isCompletada() && correcto){
			String[] tarea = new String[4];
			tarea[0] = tasks.peek().getnTarea();
			tarea[1] = tasks.peek().getFechaFin();
			tarea[2] = tasks.peek().getProyecto();
			tarea[3] = tasks.peek().getPrioridad();
			listaTareas.add(tarea);
		}
		tasks.remove();
	}
	
	
	return listaTareas;
}

public LinkedList<String[]> crearTablaTareasFuturasUser(LinkedList<Tarea> tasks, String email) {
	
	
	LinkedList<String[]> listaTareas =new LinkedList<String[]>();
	
	Date fechaactual = new Date();
    SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy/MM/dd");
    
	
	
	while(!tasks.isEmpty()){
		
		Date fechainicio;
		boolean correcto=false;
		try {
			fechainicio = dmyFormat.parse(tasks.peek().getFechaInicio());
			if(fechaactual.before(fechainicio)){ //se comprueba que la fecha actual es anterior a la fecha de inicio
				correcto=true;
			}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(tasks.peek().getEmail().equals(email) && !tasks.peek().isCompletada() && correcto){
			String[] tarea = new String[4];
			tarea[0] = tasks.peek().getnTarea();
			tarea[1] = tasks.peek().getFechaFin();
			tarea[2] = tasks.peek().getProyecto();
			tarea[3] = tasks.peek().getPrioridad();
			listaTareas.add(tarea);
		}
		tasks.remove();
	}
	
	
	return listaTareas;
}

public String getNTareaInfo(){ return tareaInfo; }
	
	
public void setNTareaInfo(String nTarea){ tareaInfo=nTarea; }

}