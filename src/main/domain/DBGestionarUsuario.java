package main.domain;

import java.util.LinkedList;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import main.persistance.DataBase;

public class DBGestionarUsuario {
	
	static DBGestionarUsuario mInstancia;
	DataBase mInstance;
	DBObject document;
	DBCursor dc;
	String emailLogin;
	DBGestionarProyectos gP=new DBGestionarProyectos();
	Email e=new Email();
	
	public DBGestionarUsuario(){
		mInstance=DataBase.getInstance();
	}
	
	public static DBGestionarUsuario getInstance(){
		if(mInstancia==null){
			mInstancia=new DBGestionarUsuario();
		}
		return mInstancia;
	}
	
	public boolean createUsuario(String user, String clave, String nombre, String apellidos, String rol, String direccion, String telefono, boolean dadoAlta){
			boolean enviado=false;
			document=new BasicDBObject();
			document.put("usuario", user);
			dc=mInstance.readUsuario(document);
			if(dc.count()==1) return enviado;
			else{
				if(clave.length()<8){return enviado;}
				else{
					document.put("clave", clave);
					document.put("nombre", nombre);
					document.put("apellidos", apellidos);
					document.put("rol", rol);
					document.put("direccion", direccion);
					document.put("telefono", telefono);
					document.put("dadoAlta", dadoAlta);
					enviado=e.enviarEmail(user, "[TodoApp] Bienvenido a TodoApp", "Gracias por usar la aplicacion TodoApp. Registro Realizado con �xito.");
					if(enviado){
						mInstance.createUsuario(document);
						LinkedList<String>users=new LinkedList<String>();
						users.add(user);
						gP.crearProyecto(users, "Bandeja de Entrada");
					}
					
					return enviado;
				}
			}
		}
	
	public Usuario recuperarUsuario(Usuario u){
		document=new BasicDBObject();
		document.put("usuario", u.getEmail());
		dc=mInstance.readUsuario(document);
		DBObject obj=dc.next();
		return new Usuario(obj.get("usuario").toString(), obj.get("clave").toString(), obj.get("nombre").toString(),obj.get("apellidos").toString(),obj.get("rol").toString(), obj.get("direccion").toString(), obj.get("telefono").toString(), (boolean)obj.get("dadoAlta"));
	}
	
	public boolean updateUsuario(String user, String clave, String nombre, String apellidos, String rol, String direccion, String telefono){
		boolean enviado=false;
		if(clave.length()>=8){
			document=new BasicDBObject();
			document.put("usuario", user);
			dc=mInstance.readUsuario(document);
			if(dc.count()==1){
				DBObject antiguo=dc.next();
				document=new BasicDBObject();
				document.put("usuario", user);
				document.put("clave", clave);
				document.put("nombre", nombre);
				document.put("apellidos", apellidos);
				document.put("rol", rol);
				document.put("direccion", direccion);
				document.put("telefono", telefono);
				document.put("dadoAlta", antiguo.get("dadoAlta"));
				enviado=e.enviarEmail(user, "[TodoApp] Usuario Modificado", "Se ha modificado su usuario");
				if(enviado) mInstance.updateUsuario(antiguo, document);
			}
		}
		return enviado;
	}
	
	public boolean cambiarContrasena(String user, String clave, String nombre, String apellidos, String rol, String direccion, String telefono){
		boolean enviado=false;
		if(clave.length()>=8){
			document=new BasicDBObject();
			document.put("usuario", user);
			dc=mInstance.readUsuario(document);
			if(dc.count()==1){
				DBObject antiguo=dc.next();
				document=new BasicDBObject();
				document.put("usuario", user);
				document.put("clave", clave);
				document.put("nombre", nombre);
				document.put("apellidos", apellidos);
				document.put("rol", rol);
				document.put("direccion", direccion);
				document.put("telefono", telefono);
				document.put("dadoAlta", antiguo.get("dadoAlta"));
				enviado=e.enviarEmail(user, "[TodoApp] Cambiar Contrase�a", "Su contrase�a ha sido cambiada. Ahora es: " + clave);
				if(enviado) mInstance.updateUsuario(antiguo, document);
			}
		}
		return enviado;
	}

	public LinkedList<Usuario> readUsuarios(){
		DBCursor users=mInstance.readAllUsuario();
		LinkedList<Usuario> usuarios=new LinkedList<Usuario>();
		DBObject u;
		while(users.hasNext()){
			u=users.next();
			usuarios.add(new Usuario(u.get("usuario").toString(), u.get("clave").toString()));
		}
		return usuarios;
	}
	
	public boolean autenticar(Usuario user){
		document=new BasicDBObject();
		document.put("usuario", user.getEmail());
		document.put("clave", user.getClave());
		DBCursor dc=mInstance.readUsuario(document);
		if(dc.count()==1)return true;
		else return false;
	}
	
	public boolean deleteUsuario(String user){
		boolean enviado=false;
		document=new BasicDBObject();
		document.put("usuario", user);
		enviado=e.enviarEmail(user, "[TodoApp] Cuenta Eliminada", "Su cuenta ha sido eliminada.");
		if(enviado) mInstance.deleteUsuario(document);
		return enviado;
	}

	public void desconectar(){
		mInstance.cerrarBD();
	}
	
	public void darAlta(){
		int i=0;
		document=new BasicDBObject();
		document.put("dadoAlta", "false");
		DBCursor dc= mInstance.readUsuario(document);
		while(dc.hasNext()){
			i++;
			DBObject document1=dc.next();
			DBObject document2=document1;
			document2.put("dadoAlta", "true");
			deleteUsuario(document1.get("usuario").toString());
			createUsuario(document2.get("usuario").toString(),document2.get("clave").toString(),document2.get("nombre").toString(), document2.get("apellidos").toString(),document2.get("rol").toString(), document2.get("direccion").toString(), document2.get("telefono").toString(),(boolean)document2.get("dadoAlta"));
			e.enviarEmail(document2.get("usuario").toString(), "[TodoApp] Dado de Alta", "Ha sido dado de alta");
		}
	}
	

	
	public boolean cambiarRol(String user, String rol){
		boolean enviado=false;
		document=new BasicDBObject();
		document.put("usuario", user);
		DBCursor dc= mInstance.readUsuario(document);
		DBObject nuevo=dc.next();
		nuevo.put("rol", rol);
		enviado=e.enviarEmail(user, "[TodoApp] Cambio de Rol", "Su nuevo rol es: "+rol);
		if(enviado) mInstance.updateUsuario(document, nuevo);
	    return enviado;
	}
	
	public String recuperarClave(String email){
		document=new BasicDBObject();
		document.put("usuario", email);
		DBCursor dc= mInstance.readUsuario(document);
		DBObject o=dc.next();
		//e.enviarEmail(email, "[TodoApp] Recuperar clave", "Su clave es la siguiente: "+o.get("clave").toString());
		return o.get("clave").toString();
	}
	
	public void cambiarClave(String user, String clave){
		document=new BasicDBObject();
		document.put("usuario", user);
		DBCursor dc= mInstance.readUsuario(document);
		DBObject nuevo=dc.next();
		nuevo.put("clave", clave);
		mInstance.updateUsuario(document, nuevo);
		e.enviarEmail(user, "[TodoApp] Cambio de Clave", "Su nueva clave es "+clave);
	}

	public String getRol(Usuario user){
	    document=new BasicDBObject();
	    document.put("usuario", user.getEmail());
	    document.put("clave", user.getClave());
	    DBCursor dc=mInstance.readUsuario(document);
	    return dc.next().get("rol").toString();
	  }
	
	public String getNombre(Usuario user){
	    document=new BasicDBObject();
	    document.put("usuario", user.getEmail());
	    document.put("clave", user.getClave());
	    DBCursor dc=mInstance.readUsuario(document);
	    return dc.next().get("nombre").toString();
	  }
	
	public boolean getDadoAlta(Usuario user){
	    document=new BasicDBObject();
	    document.put("usuario", user.getEmail());
	    document.put("clave", user.getClave());
	    DBCursor dc=mInstance.readUsuario(document);
	    return (boolean) dc.next().get("dadoAlta");
	  }
	
	public String getApellidos(Usuario user){
	    document=new BasicDBObject();
	    document.put("usuario", user.getEmail());
	    document.put("clave", user.getClave());
	    DBCursor dc=mInstance.readUsuario(document);
	    return dc.next().get("apellidos").toString();
	  }
	
	public String getDireccion(Usuario user){
	    document=new BasicDBObject();
	    document.put("usuario", user.getEmail());
	    document.put("clave", user.getClave());
	    DBCursor dc=mInstance.readUsuario(document);
	    return (String) dc.next().get("direccion");
	  }
	
	public String getTelefono(Usuario user){
	    document=new BasicDBObject();
	    document.put("usuario", user.getEmail());
	    document.put("clave", user.getClave());
	    DBCursor dc=mInstance.readUsuario(document);
	    return dc.next().get("telefono").toString();
	  }
	
	public String getEmail(Usuario user){
	    document=new BasicDBObject();
	    document.put("usuario", user.getEmail());
	    document.put("clave", user.getClave());
	    DBCursor dc=mInstance.readUsuario(document);
	    return dc.next().get("usuario").toString();
	  }
	
	public LinkedList<String[]> crearTablaDadosAlta(LinkedList<Usuario> users) {
		LinkedList<String[]> listaUsuariosVista =new LinkedList<String[]> ();
		
		for (int i=0;i<users.size();i++){
			Usuario u = users.get(i);
			boolean dadoAlta = getDadoAlta(u);
			String[] datosUsuarios = new String[2];
			datosUsuarios[0] = u.getEmail();
			datosUsuarios[1] = getNombre(u);
			if(!dadoAlta) listaUsuariosVista.add(datosUsuarios);
		}
		return listaUsuariosVista;
	}
	
	public LinkedList<String[]> crearTablaDadosBaja(LinkedList<Usuario> users) {
		LinkedList<String[]> listaUsuariosVista =new LinkedList<String[]> ();
		
		for (int i=0;i<users.size();i++){
			Usuario u = users.get(i);
			boolean dadoAlta = getDadoAlta(u);
			String[] datosUsuarios = new String[2];
			datosUsuarios[0] = u.getEmail();
			datosUsuarios[1] = getNombre(u);
			if(dadoAlta) listaUsuariosVista.add(datosUsuarios);
		}
		return listaUsuariosVista;
	}
	
	public LinkedList<String[]> crearTablaRoles(LinkedList<Usuario> users) {
				
		
		LinkedList<String[]> listaUsuariosVistaRol =new LinkedList<String[]>();
		
		
		for (int i=0;i<users.size();i++){
			Usuario u = users.get(i);
			boolean dadoAlta = getDadoAlta(u);
			String[] rolesUsuarios = new String[2];
			rolesUsuarios[0] = u.getEmail();
			rolesUsuarios[1] = getRol(u);
			if(dadoAlta) listaUsuariosVistaRol.add(rolesUsuarios);
		}
		
		
		return listaUsuariosVistaRol;
	}
	
	public LinkedList<String> crearTablaUsuarios(LinkedList<Usuario> users) {
	    LinkedList<String> listaUsuariosVista =new LinkedList<String> ();
	    
	    for (int i=0;i<users.size();i++){
	      Usuario u = users.get(i);
	      String datosUsuarios = u.getEmail();
	      listaUsuariosVista.add(datosUsuarios);
	    }
	    return listaUsuariosVista;
	  }
	
	public boolean darDeAlta(String email){
		boolean enviado=false;
	    document=new BasicDBObject();
	    document.put("usuario", email);
	    DBCursor dc=mInstance.readUsuario(document);
	    DBObject antiguo=dc.next();
	    DBObject nuevo=antiguo;
	    nuevo.put("dadoAlta", true);
	    enviado=e.enviarEmail(email, "[TodoApp] Dado de Alta", "Ha sido dado de alta");
	    if(enviado) mInstance.updateUsuario(document, nuevo);
		return enviado;
	  }
	
	public boolean darDeBaja(String email){
		boolean enviado=false;
	    document=new BasicDBObject();
	    document.put("usuario", email);
	    DBCursor dc=mInstance.readUsuario(document);
	    DBObject antiguo=dc.next();
	    DBObject nuevo=antiguo;
	    nuevo.put("dadoAlta", false);
	    enviado=e.enviarEmail(email, "[TodoApp] Dado de Baja", "Ha sido dado de baja");
	    if(enviado) mInstance.updateUsuario(document, nuevo);
		return enviado;
	  }
	
	public boolean modificar(Usuario uV, Usuario uN){
		this.deleteUsuario(uV.getEmail());
		this.createUsuario(uN.getEmail(), uN.getClave(), uN.getNombre(), uN.getApellidos(), uN.getRol(), uN.getDireccion(), uN.getTelefono(), uN.getAlta());
		e.enviarEmail(uN.getEmail(), "[TodoApp] Cuenta Modificada", "Ha sido modificada su cuenta");
		return true;
	}
}
