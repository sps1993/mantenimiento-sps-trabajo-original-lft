package main.domain;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

public class Email {

	public Email(){
		super();
	}
	
	public boolean enviarEmail(String emailDestino, String asunto, String mensaje){
		Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("todoAppGrupoSPS@gmail.com", "migrupomola5");
                    }
                });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("todoAppGrupoSPS@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(emailDestino));
            message.setSubject(asunto);
            message.setText(mensaje);
            Transport.send(message);
            return true;
        } catch (MessagingException e) {
            //throw new RuntimeException(e);
        	return false;
        }
	}
}
