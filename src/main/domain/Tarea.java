package main.domain;

public class Tarea {
	String email, nTarea, prioridad, proyecto, fechaInicio, fechaFin, notas;
	boolean completada;
	
	public Tarea(String em, String nT, String prio, String proj, String fI, String fF, String note, boolean comp){
		this.email=em;
		this.nTarea=nT;
		this.prioridad=prio;
		this.proyecto=proj;
		this.fechaInicio=fI;
		this.fechaFin=fF;
		this.notas=note;
		this.completada=comp;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getnTarea() {
		return nTarea;
	}
	public void setnTarea(String nTarea) {
		this.nTarea = nTarea;
	}
	public String getPrioridad() {
		return prioridad;
	}
	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}
	public String getProyecto() {
		return proyecto;
	}
	public void setProyecto(String proyecto) {
		this.proyecto = proyecto;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getNotas() {
		return notas;
	}
	public void setNotas(String notas) {
		this.notas = notas;
	}
	public boolean isCompletada() {
		return completada;
	}
	public void setCompletada(boolean completada) {
		this.completada = completada;
	}
}
