package main.domain;

public class Usuario {
	String email="", clave="", nombre="", apellidos="", rol="", direccion="", telefono="";
	boolean alta=false;

	public Usuario(String em, String pwd){
		this.email=em;
		this.clave=pwd;
	}
	
	public Usuario(String em, String pwd, String nomb, String ape, String rl, String dir, String tfn, boolean al){
		this.email=em;
		this.clave=pwd;
		this.nombre=nomb;
		this.apellidos=ape;
		this.rol=rl;
		this.direccion=dir;
		this.telefono=tfn;
		this.alta=al;
	}
	
	public String getEmail(){return this.email;}
	public String getClave(){return this.clave;}
	public String getRol(){return this.rol;}
	public boolean getAlta(){return this.alta;}
	public void setEmail(String email){this.email = email;}
	public void setClave(String clave){this.clave = clave;}
	public void setRol(String rol){this.rol = rol;}
	public void setAlta(boolean alta){this.alta = alta;}
	public String getNombre(){return nombre;}
	public void setNombre(String nombre) {this.nombre = nombre;}
	public String getApellidos() {return apellidos;}
	public void setApellidos(String apellidos) {this.apellidos = apellidos;}
	public String getDireccion() {return direccion;}
	public void setDireccion(String direccion) {this.direccion = direccion;}
	public String getTelefono() {return telefono;}
	public void setTelefono(String telefono) {this.telefono = telefono;}
}
