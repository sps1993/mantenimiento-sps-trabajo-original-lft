package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import main.domain.DBGestionarUsuario;
import main.domain.DatosLogin;
import main.domain.Usuario;


@Controller
public class CambiarContraseñaController extends HttpServlet{

	
	private static final long serialVersionUID = 1L;


	@RequestMapping("Cambiar_Contrasena")
	public ModelAndView cambiarContraseña(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		boolean correoEnviado=false, continuar=true;
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/CambiarContraseña.jsp");
		String contrasenaVieja = request.getParameter("contrasenaActual");
		String nuevaContrasena = request.getParameter("contrasenaNueva");
		String repNuevaContrasena = request.getParameter("contrasenaNuevaRep");
		
		String feedback="";
		
		DBGestionarUsuario instancia = new DBGestionarUsuario();
		DatosLogin dl = new DatosLogin();
		String email = dl.getEmailLogin();
		String contra = dl.getContrasenaLogin(email);
		
		if(contrasenaVieja.isEmpty() || nuevaContrasena.isEmpty() || repNuevaContrasena.isEmpty()){
			feedback="Todos los campos deben ser rellenados.";
			model.addObject("comienzoMensajeError", "Error. ");
			model.addObject("MensajeError", feedback);
		}
		else{
			if(contrasenaVieja.equals(contra)){
				if(nuevaContrasena.equals(repNuevaContrasena)){
					if(nuevaContrasena.length()<8){
						feedback="La contraseña debe tener 8 caracteres como mínimo.";
						model.addObject("comienzoMensajeError", "Error. ");
						model.addObject("MensajeError", feedback);
					}
					else{
						LinkedList<Usuario> lista = instancia.readUsuarios();
						
						for(int i=0; i<lista.size();i++){
							if(lista.get(i).getEmail().equals(email) && lista.get(i).getClave().equals(contrasenaVieja) && continuar){
								Usuario u = new Usuario(email,contrasenaVieja);
								instancia.getNombre(u);
								correoEnviado=instancia.cambiarContrasena(email, nuevaContrasena, instancia.getNombre(u), instancia.getApellidos(u), instancia.getRol(u), instancia.getDireccion(u), instancia.getTelefono(u));
								if(correoEnviado){
									feedback="La contraseña se ha cambiado correctamente.";
									model.addObject("comienzoMensaje", "Completado. ");
									model.addObject("Mensaje", feedback);
									continuar=false;
								}
								else{
									feedback="No se ha podido cambiar la contraseña. Por favor, vuelva a intentarlo más tarde.";
									model.addObject("comienzoMensajeError", "Error. ");
									model.addObject("MensajeError", feedback);
								}
								
							}
							else{
								if(correoEnviado){
									feedback="La contraseña no es la original. Por favor, inserte la contraseña correcta.";
									model.addObject("comienzoMensajeError", "Error. ");
									model.addObject("MensajeError", feedback);
								}
								
							}
						}
					}
				}
				else{
					feedback="Las contraseñas deben coincidir.";
					model.addObject("comienzoMensajeError", "Error. ");
					model.addObject("MensajeError", feedback);
				}
			}
			else{
				feedback="La contraseña actual no coincide. Por favor, introduzca la misma contraseña en los campos.";
				model.addObject("comienzoMensajeError", "Error. ");
				model.addObject("MensajeError", feedback);
			}
			
		}

		
		
		return model;
	}

}
