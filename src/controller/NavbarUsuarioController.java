package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import main.domain.DBGestionarUsuario;
import main.domain.DatosLogin;
import main.domain.Usuario;
import main.domain.DBGestionarTarea;
import main.domain.Tarea;

@Controller
public class NavbarUsuarioController extends HttpServlet{
	private static final long serialVersionUID = 1L;
	public LinkedList<String[]> cargarTareasCompletas(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasCompletadasUser(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasActuales(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasActualesUser(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasFuturas(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasFuturasUser(tasks,email);
		
		return listaTareas;
	}	
	/**M�dulo Gesti�n de Usuarios**/
	
	@RequestMapping("HomeUser")
	public ModelAndView mostrarPantallaPrincipal(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/vista_pantallaPrincipalUsuarios.jsp");
		DatosLogin dl = new DatosLogin();
		
		LinkedList<String[]> listaTareasCompletas = cargarTareasCompletas(dl.getEmailLogin());
		LinkedList<String[]> listaTareasActuales = cargarTareasActuales(dl.getEmailLogin());
		LinkedList<String[]> listaTareasFuturas = cargarTareasFuturas(dl.getEmailLogin());
		
		model.addObject("tareasCompletas",listaTareasCompletas);
		model.addObject("tareasActuales",listaTareasActuales);
		model.addObject("tareasFuturas",listaTareasFuturas);
		return model;
		
	}
	@RequestMapping("NewProjectUser")
	public ModelAndView crearProyecto(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/vista_crearProyecto.jsp");
		return model;
		
	}
	@RequestMapping("ChangePasswordUser")
	public ModelAndView cambiarContra(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/CambiarContrase�a.jsp");
		return model;
		
	}
	

	/**M�dulo Opciones**/
	
	@RequestMapping("VerPerfilUsuario")
	public ModelAndView verPerfilAdmin(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/VerPerfilUsuario.jsp");
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		DatosLogin dl = new DatosLogin();
		String email = dl.getEmailLogin();
		LinkedList<Usuario> users = usuarios.readUsuarios();
		int i=0;
		Usuario usuario;
		while(!(((usuario=(users.get(i))).getEmail()).equals(email))){
			i++;	
		}
		email = usuario.getEmail();
		String nombre = usuarios.getNombre(usuario);
		String apellidos = usuarios.getApellidos(usuario);
		String direccion = usuarios.getDireccion(usuario);
		String telefono = usuarios.getTelefono(usuario);
		model.addObject("nombre", nombre);
		model.addObject("email", email);
		model.addObject("apellidos", apellidos);
		model.addObject("direccion", direccion);
		model.addObject("telefono", telefono);
		return model;
		
	}
	
	@RequestMapping("CerrarSesionUser")
	public ModelAndView cerrarSesion(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/index.jsp");
		DBGestionarUsuario.getInstance().desconectar();
		
		model.addObject("comienzoMensaje", "Completado.");
		model.addObject("Mensaje", "Ha cerrado sesi�n correctamente.");
		return model;
	}
}
