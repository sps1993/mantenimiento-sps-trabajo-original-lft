package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import main.domain.DBGestionarProyectos;
import main.domain.DBGestionarTarea;
import main.domain.DBGestionarUsuario;
import main.domain.DatosLogin;
import main.domain.Tarea;
import main.domain.Usuario;
//import main.domain.Usuario;

@Controller
public class NavbarAdminController extends HttpServlet{
	private static final long serialVersionUID = 1L;
	
	@RequestMapping("HomeAdmin")
	public ModelAndView menuPrincipal(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/vista_admin.jsp");
		return model;
	}
	
	/**M�dulo Gesti�n de Usuarios**/
	
	@RequestMapping("AltasUsuarios")
	public ModelAndView darDeAlta(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/DarAltas_admin.jsp");
		
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		
		LinkedList<String[]> listaUsuariosVista =usuarios.crearTablaDadosAlta(users);
		model.addObject("lista", listaUsuariosVista);
		return model;
	}
	
	@RequestMapping("BajasUsuarios")
	public ModelAndView darDeBaja(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/DarBajas_admin.jsp");
		
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		
		LinkedList<String[]> listaUsuariosVista =usuarios.crearTablaDadosBaja(users);
		model.addObject("lista", listaUsuariosVista);
		
		return model;
	}
	
	@RequestMapping("CambiarRoles")
	public ModelAndView cambiarRoles(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{

		ModelAndView model = new ModelAndView("/WEB-INF/jsp/CambiarRoles_admin.jsp");
		
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		LinkedList<Usuario> user =usuarios.readUsuarios();
		while(user.isEmpty()){
			Usuario x = user.pop();
			System.out.println("Email: " + x.getEmail() + " Clave: " + x.getClave());
		}
		LinkedList<String[]> listaUsuariosVistaRol =usuarios.crearTablaRoles(users);
		model.addObject("lista", listaUsuariosVistaRol);
		return model;
	}
	
	/**M�dulo Gesti�n de Tareas**/
	
	@RequestMapping("CrearProyectoAdmin")
	public ModelAndView crearProyectoAdmin(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{

		ModelAndView model = new ModelAndView("/WEB-INF/jsp/vista_crearProyecto_admin.jsp");

		return model;
	}
	
	@RequestMapping("CrearTareaAdmin")
	public ModelAndView crearTareaAdmin(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/vista_crearTarea_admin.jsp");
		
		DBGestionarProyectos gestProy=new DBGestionarProyectos();
		DatosLogin dl = new DatosLogin();
		String email = dl.getEmailLogin();
		LinkedList<String> emails= new LinkedList <String>();
		emails.add(email);		
		LinkedList<String>proyectos=gestProy.leerProyectos(emails);
		model.addObject("proyectos", proyectos);

		return model;
	}
	
	@RequestMapping("VerTareasAdmin")
	public ModelAndView listarTareas(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		//ModelAndView model = new ModelAndView("/WEB-INF/jsp/tareasUsuariosAdmin.jsp");
		DatosLogin dl = new DatosLogin();
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/tareasAdmin.jsp");
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		boolean userEncontrado=false;
		LinkedList<String> listaUsuariosVista =usuarios.crearTablaUsuarios(users);
		String userEmail;
		String userEmailU = dl.getEmailLogin();
		
		LinkedList<String[]> listaTareasCompletas = cargarTareasCompletas(userEmailU);
		LinkedList<String[]> listaTareasActuales = cargarTareasActuales(userEmailU);
		LinkedList<String[]> listaTareasFuturas = cargarTareasFuturas(userEmailU);
		model.addObject("tareasCompletas",listaTareasCompletas);
		model.addObject("tareasActuales",listaTareasActuales);
		model.addObject("tareasFuturas",listaTareasFuturas);
		
		return model;	
	}
	
	
	
	public LinkedList<String[]> cargarTareasCompletas(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasCompletadasUser(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasActuales(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasActualesUser(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasFuturas(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasFuturasUser(tasks,email);
		
		return listaTareas;
	}
	
	@RequestMapping("VerUsuarios")
	public ModelAndView listarUsuarios(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		
		LinkedList<String> listaUsuariosVista =usuarios.crearTablaUsuarios(users);
		for(int i=0; i<listaUsuariosVista.size(); i++){
		}
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/ListarUsuarios.jsp");
		model.addObject("listaTodos", listaUsuariosVista);
		return model;
	}

	/**M�dulo Opciones**/
	
	@RequestMapping("VerPerfilAdmin")
	public ModelAndView verPerfilAdmin(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/MiPerfilAdmin.jsp");
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		DatosLogin dl = new DatosLogin();
		String email = dl.getEmailLogin();
		LinkedList<Usuario> users = usuarios.readUsuarios();
		int i=0;
		Usuario usuario;
		while(!(((usuario=(users.get(i))).getEmail()).equals(email))){
			i++;	
		}
		email = usuario.getEmail();
		String nombre = usuarios.getNombre(usuario);
		String apellidos = usuarios.getApellidos(usuario);
		String direccion = usuarios.getDireccion(usuario);
		String telefono = usuarios.getTelefono(usuario);
		model.addObject("nombre", nombre);
		model.addObject("email", email);
		model.addObject("apellidos", apellidos);
		model.addObject("direccion", direccion);
		model.addObject("telefono", telefono);
		return model;
		
	}
	
	@RequestMapping("CerrarSesion")
	public ModelAndView cerrarSesion(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/index.jsp");
		DBGestionarUsuario.getInstance().desconectar();
		
		model.addObject("comienzoMensaje", "Completado.");
		model.addObject("Mensaje", "Ha cerrado sesi�n correctamente.");
		/*Codigo para desconectarse (Deslogueo)*/
		return model;
	}
}
