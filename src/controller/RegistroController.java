package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import main.domain.DBGestionarTarea;
import main.domain.DBGestionarUsuario;
import main.domain.DatosLogin;
import main.domain.Usuario;


@Controller
public class RegistroController extends HttpServlet{

	private static final long serialVersionUID = 1L;
	private DBGestionarTarea gT=new DBGestionarTarea();
	@RequestMapping("registrar")
	public ModelAndView registro(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		return new ModelAndView("/WEB-INF/jsp/Registrar.jsp");
	}
	
	@RequestMapping("reg")
	public ModelAndView registrar(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		
		ModelAndView model = new ModelAndView();
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		boolean existe=false;
		boolean mostrarMensajeAlert=false,correoEnviado=false, registrado = false;
		
		String user=request.getParameter("userEmailReg");
		String pass=request.getParameter("contrasenaReg");
		String pass2=request.getParameter("contrasenaReg2");
		
		String mensaje="";
		if(!user.equals("") && !pass.equals("")){
			while(!users.isEmpty()){
				if(users.poll().getEmail().equals(user)){
					existe=true;
					mostrarMensajeAlert=true;
					mensaje="El usuario ya existe";
					model = new ModelAndView("/WEB-INF/jsp/Registrar.jsp");
					if(mostrarMensajeAlert){
						model.addObject("comienzoMensajeError", "Error.");
						model.addObject("MensajeError", mensaje);
					}
				}
			}
			if(!existe){
				
				if(pass.equals(pass2)){
					if(pass.length()>=8){
						mostrarMensajeAlert=true;
						correoEnviado=usuarios.createUsuario(user, pass, "John", "Doe", "Usuario", "Calle sn", "000000000", false);
						mensaje="Usuario registrado correctamente.";
						
						if(correoEnviado){
							model = new ModelAndView("/index.jsp");
							if(mostrarMensajeAlert){
								model.addObject("comienzoMensaje", "Completado.");
								model.addObject("Mensaje", mensaje);
							}
						}
						else{
							model = new ModelAndView("/WEB-INF/jsp/Registrar.jsp");
							mensaje="El correo no existe. Por favor, introduzca un correo v�lido.";
							if(mostrarMensajeAlert){
								model.addObject("comienzoMensajeError", "Error.");
								model.addObject("MensajeError", mensaje);
							}
						}
					}
					else{
						mensaje="La longitud m�nima de la contrase�a debe ser de 8 caracteres.";
						model = new ModelAndView("/WEB-INF/jsp/Registrar.jsp");
						mostrarMensajeAlert=true;
						if(mostrarMensajeAlert){
							model.addObject("comienzoMensajeError", "Error.");
							model.addObject("MensajeError", mensaje);
						}

					}
					
				}
				else{
					mensaje="Las password no son iguales.";
					model = new ModelAndView("/WEB-INF/jsp/Registrar.jsp");
					mostrarMensajeAlert=true;
					if(mostrarMensajeAlert){
						model.addObject("comienzoMensajeError", "Error.");
						model.addObject("MensajeError", mensaje);
					}
					

				}
			}
		}else{
			mensaje="Introduzca el email y la contrase�a.";
			mostrarMensajeAlert=true;
			model = new ModelAndView("/WEB-INF/jsp/Registrar.jsp");
			if(mostrarMensajeAlert){
				model.addObject("comienzoMensajeError", "Error.");
				model.addObject("MensajeError", mensaje);
			}
			

		}

		return model;
		
	}
	
	@RequestMapping("entrar")
	public ModelAndView prueba(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException, InstantiationException, IllegalAccessException{
		String mensaje;
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		DatosLogin dl = new DatosLogin();
		ModelAndView model;
		PrincipalUsuarioController puc = new PrincipalUsuarioController();
		if(request.getParameter("userEmailLogin").equals("") || request.getParameter("contrasenaLogin").equals("")){
			mensaje = "Los campos email y contrase�a no pueden estar vac�os.";
			model = new ModelAndView("/index.jsp");
			model.addObject("comienzoMensajeError", "Error.");
			model.addObject("MensajeError", mensaje);
		}
		else{
			if(request.getParameter("contrasenaLogin").length()<8){
				mensaje = "La contrase�a debe ser, como m�nimo, de 8 caracteres. Contrase�a err�nea.";
				model = new ModelAndView("/index.jsp");
				model.addObject("comienzoMensajeError", "Error.");
				model.addObject("MensajeError", mensaje);
			}
			else{
				dl.setEmailLogin(request.getParameter("userEmailLogin"));
				Usuario u = new Usuario(request.getParameter("userEmailLogin"), request.getParameter("contrasenaLogin"));
				boolean autenticado = usuarios.autenticar(u);
				if(autenticado){
					String emailLogin = dl.getEmailLogin();
					String rol = usuarios.getRol(u);
					if(rol.equals("admin")){
						if(usuarios.getDadoAlta(u)){
							gT.avisos(usuarios.getEmail(u));
							model = new ModelAndView("/WEB-INF/jsp/vista_admin.jsp");
							model.addObject("emailLogueado", emailLogin);
						}
						else{
							mensaje = "No est� dado de alta. Por favor, espere a que un administrador le de de alta en el sistema.";
							model = new ModelAndView("/index.jsp");
							model.addObject("comienzoMensajeError", "Error.");
							model.addObject("MensajeError", mensaje);
						}
					}
					else{
						if(usuarios.getDadoAlta(u)){
							gT.avisos(usuarios.getEmail(u));
							model = new ModelAndView("/WEB-INF/jsp/vista_pantallaPrincipalUsuarios.jsp");
							LinkedList<String[]> listaTareasCompletas = puc.cargarTareasCompletasUser(emailLogin);
							LinkedList<String[]> listaTareasActuales = puc.cargarTareasActualesUser(emailLogin);
							LinkedList<String[]> listaTareasFuturas = puc.cargarTareasFuturasUser(emailLogin);
							model.addObject("tareasCompletas",listaTareasCompletas);
							model.addObject("tareasActuales",listaTareasActuales);
							model.addObject("tareasFuturas",listaTareasFuturas);
						}
						else{
							mensaje = "No est� dado de alta. Por favor, espere a que un administrador le de de alta en el sistema.";
							model = new ModelAndView("/index.jsp");
							model.addObject("comienzoMensajeError", "Error.");
							model.addObject("MensajeError", mensaje);
						}
					}
				}
				else{
					mensaje = "El usuario/contrase�a es err�neo. Por favor, rev�sela";
					model = new ModelAndView("/index.jsp");
					model.addObject("comienzoMensajeError", "Error.");
					model.addObject("MensajeError", mensaje);
				}
			}
			}
			
		
		return model;
	}
}
