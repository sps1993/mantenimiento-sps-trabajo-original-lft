package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import main.domain.DBGestionarUsuario;
import main.domain.DatosLogin;
import main.domain.Usuario;


@Controller
class ModificarPerfilUsuarioController {
	@RequestMapping("perfilUsuarioModificar")
	public ModelAndView verPerfilAdmin(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/MiPerfilUsuario.jsp");
		String nombre = request.getParameter("inputNombre");
		String apellidos = request.getParameter("inputApellidos");
		String direccion = request.getParameter("inputDireccion");
		String telefono = request.getParameter("inputTelefono");
		
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		DatosLogin dl = new DatosLogin();
		String email = dl.getEmailLogin();
		LinkedList<Usuario> users = usuarios.readUsuarios();
		int i=0;
		Usuario usuario;
		while(!(((usuario=(users.get(i))).getEmail()).equals(email))){
			i++;	
		}
		email = usuario.getEmail();
		usuarios.updateUsuario(usuario.getEmail(), usuario.getClave(), nombre, apellidos, usuarios.getRol(usuario), direccion, telefono);
		model.addObject("nombre", nombre);
		model.addObject("email", email);
		model.addObject("apellidos", apellidos);
		model.addObject("direccion", direccion);
		model.addObject("telefono", telefono);
		
		
		return model;
	}
}