package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import main.domain.DBGestionarProyectos;
import main.domain.DBGestionarTarea;
import main.domain.DBGestionarUsuario;
import main.domain.DatosLogin;
import main.domain.Tarea;
import main.domain.Usuario;

@Controller
public class CrearProyectoController {
	

	
	@RequestMapping("crearNuevoProyecto")
	public ModelAndView creaProyecto(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/vista_crearProyecto.jsp");
		DBGestionarProyectos gP=new DBGestionarProyectos();
		DBGestionarUsuario gU = new DBGestionarUsuario();
		//DatosLogin dl = new DatosLogin();
		String propietarios = request.getParameter("propietariosProyecto");
		String mensaje="";
		String[] prop = propietarios.split(", ");
		LinkedList<Usuario> usuarios = gU.readUsuarios();
		LinkedList<String> propietariosProyecto = new LinkedList<String>();
		int contadorExist=0;
		boolean correoEnviado=false;
		
		for(int i=0; i<prop.length; i++){
			propietariosProyecto.add(prop[i]);
		}
		
		for(int i=0; i<usuarios.size();i++){
			for(int j=0; j<propietariosProyecto.size();j++){
				if(propietariosProyecto.get(j).equals(usuarios.get(i).getEmail())){
					contadorExist++;
				}
			}
		}
		
				
		if(propietariosProyecto.size()==contadorExist){
			correoEnviado=gP.crearProyecto(propietariosProyecto, request.getParameter("nuevoProyecto"));
		}
		if(correoEnviado){
			mensaje="El proyecto " + request.getParameter("nuevoProyecto") + " se ha creado correctamente.";
			model.addObject("comienzoMensaje", "Completado. ");
			model.addObject("Mensaje", mensaje);
		}
		else{
			mensaje="No se ha podido crear su proyecto. Por favor, vuelva a intentarlo m�s tarde.";
			model.addObject("comienzoMensajeError", "Error.");
			model.addObject("MensajeError", mensaje);
		}
		return model;
	}
	

	
}
