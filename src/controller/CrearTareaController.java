package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import main.domain.DBGestionarProyectos;
import main.domain.DBGestionarTarea;
import main.domain.DBGestionarUsuario;
import main.domain.DatosLogin;
import main.domain.Tarea;
import main.domain.Usuario;


@Controller
public class CrearTareaController extends HttpServlet{
	
	private static final long serialVersionUID = 1L;

	@RequestMapping("ntUser")
	public ModelAndView crearTareaNueva(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/vista_crearTarea.jsp");

		DBGestionarTarea tareas = new DBGestionarTarea();
		boolean correoEnviado=false;
		DatosLogin dl = new DatosLogin();
		String email = dl.getEmailLogin();
		LinkedList<String> emails= new LinkedList <String>();
		emails.add(email);		
		String nombre = request.getParameter("nombreCT");
		String prioridad = request.getParameter("prioridadCT");
		String fechaInicio = request.getParameter("fechaInicioCT");
		String fechaFin = request.getParameter("fechaFinCT");
		String proyecto = request.getParameter("proyectoCT");
		String notas = request.getParameter("notasCT");
		boolean completada = request.getParameter("completadaCrearTarea") != null;
		boolean existe = false;
		String mensaje="";
		LinkedList<Tarea> tasks = tareas.readTareas();
		
		while(!tasks.isEmpty()){
			if((tasks.peek().getEmail().equals(email))&&(tasks.peek().getnTarea().equals(nombre))){
				existe=true;
				mensaje="La tarea ya existe";
				model.addObject("comienzoMensajeError", "Error. ");
				model.addObject("MensajeError", mensaje);
			}
			tasks.poll();
		}
		
		if(!existe){
			correoEnviado=tareas.createTarea(email, nombre, prioridad, proyecto, completada, fechaInicio, fechaFin, notas);
			mensaje="Tarea creada correctamente.";
			if(correoEnviado){
				model.addObject("comienzoMensaje", "Completado.");
				model.addObject("Mensaje", mensaje);
			}
			else{
				mensaje="No se ha podido crear la tarea. Por favor, vuelva a intentarlo m�s tarde.";
				model.addObject("comienzoMensajeError", "Error.");
				model.addObject("MensajeError", mensaje);
			}
			
		}
		
		
		DBGestionarProyectos gestProy=new DBGestionarProyectos();
		LinkedList<String>proyectos=gestProy.leerProyectos(emails);
		model.addObject("proyectos", proyectos);

		return model;
		
		
	}
	
	

}
