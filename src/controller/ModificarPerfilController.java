package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import main.domain.DBGestionarUsuario;
import main.domain.DatosLogin;
import main.domain.Usuario;



@Controller
class ModificarPerfilController {
	@RequestMapping("PerfilModificar")
	public ModelAndView verPerfilAdmin(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model;
		String nombre = request.getParameter("inputNombre");
		String apellidos = request.getParameter("inputApellidos");
		String direccion = request.getParameter("inputDireccion");
		String telefono = request.getParameter("inputTelefono");
		boolean correoEnviado;
		String mensaje;
		
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		DatosLogin dl = new DatosLogin();
		String email = dl.getEmailLogin();
		LinkedList<Usuario> users = usuarios.readUsuarios();
		int i=0;
		Usuario usuario;
		while(!(((usuario=(users.get(i))).getEmail()).equals(email))){
			i++;	
		}
		email = usuario.getEmail();
		
		if(nombre.equals("") || apellidos.equals("") || direccion.equals("") || telefono.equals("") ){
			model= new ModelAndView("/WEB-INF/jsp/ModificarPerfilAdmin.jsp");
			mensaje="Alguno de los campos est� vac�o. Por favor, rellene todos los campos.";
			model.addObject("comienzoMensajeError", "Error. ");
			model.addObject("MensajeError", mensaje);
		}
		else{
			model= new ModelAndView("/WEB-INF/jsp/MiPerfilAdmin.jsp");
			correoEnviado=usuarios.updateUsuario(usuario.getEmail(), usuario.getClave(), nombre, apellidos, usuarios.getRol(usuario), direccion, telefono);
			
			model.addObject("nombre", nombre);
			model.addObject("email", email);
			model.addObject("apellidos", apellidos);
			model.addObject("direccion", direccion);
			model.addObject("telefono", telefono);
			
			if(correoEnviado){
				mensaje="Perfil modificado con �xito.";
				model.addObject("comienzoMensaje", "Completado. ");
				model.addObject("Mensaje", mensaje);
			}
			else{
				mensaje="No se ha podido modificar su perfil. Por favor, vuelva a intentarlo m�s tarde.";
				model.addObject("comienzoMensajeError", "Error.");
				model.addObject("MensajeError", mensaje);
			}
		}

		return model;
	}
}
