package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import main.domain.DBGestionarProyectos;
import main.domain.DBGestionarTarea;
import main.domain.DBGestionarUsuario;
import main.domain.DatosLogin;
import main.domain.Tarea;
import main.domain.Usuario;


@Controller
public class GestionTareasAdminController extends HttpServlet{
	
	private static final long serialVersionUID = 1L;

	@RequestMapping("ntAdmin")
	public ModelAndView crearTareaNueva(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/vista_crearTarea_admin.jsp");

		DBGestionarTarea tareas = new DBGestionarTarea();
		boolean correoEnviado=false;
		DatosLogin dl = new DatosLogin();
		String email = dl.getEmailLogin();
		LinkedList<String> emails= new LinkedList <String>();
		emails.add(email);		
		String nombre = request.getParameter("nombreCT");
		String prioridad = request.getParameter("prioridadCT");
		String fechaInicio = request.getParameter("fechaInicioCT");
		String fechaFin = request.getParameter("fechaFinCT");
		String proyecto = request.getParameter("proyectoCT");
		String notas = request.getParameter("notasCT");
		boolean completada = request.getParameter("completadaCrearTarea") != null;
		boolean existe = false;
		String mensaje="";
		LinkedList<Tarea> tasks = tareas.readTareas();
		
		while(!tasks.isEmpty()){
			if((tasks.peek().getEmail().equals(email))&&(tasks.peek().getnTarea().equals(nombre))){
				existe=true;
				mensaje="La tarea ya existe";
				model.addObject("comienzoMensajeError", "Error. ");
				model.addObject("MensajeError", mensaje);
			}
			tasks.poll();
		}
		
		if(!existe){
			correoEnviado=tareas.createTarea(email, nombre, prioridad, proyecto, completada, fechaInicio, fechaFin, notas);
			mensaje="Tarea creada correctamente.";
			if(correoEnviado){
				model.addObject("comienzoMensaje", "Completado.");
				model.addObject("Mensaje", mensaje);
			}
			else{
				mensaje="No se ha podido crear la tarea. Por favor, vuelva a intentarlo m�s tarde.";
				model.addObject("comienzoMensajeError", "Error.");
				model.addObject("MensajeError", mensaje);
			}
			
		}
		
		
		DBGestionarProyectos gestProy=new DBGestionarProyectos();
		LinkedList<String>proyectos=gestProy.leerProyectos(emails);
		model.addObject("proyectos", proyectos);

		return model;
		
		
	}
	
	
	@RequestMapping("NuevoProyectoAdmin")
	public ModelAndView creaProyecto(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/vista_crearProyecto_admin.jsp");
		DBGestionarProyectos gP=new DBGestionarProyectos();
		DBGestionarUsuario gU = new DBGestionarUsuario();
		//DatosLogin dl = new DatosLogin();
		String propietarios = request.getParameter("propietariosProyecto");
		String mensaje="";
		String[] prop = propietarios.split(", ");
		LinkedList<Usuario> usuarios = gU.readUsuarios();
		LinkedList<String> propietariosProyecto = new LinkedList<String>();
		int contadorExist=0;
		boolean correoEnviado=false;
		
		for(int i=0; i<prop.length; i++){
			propietariosProyecto.add(prop[i]);
		}
		
		for(int i=0; i<usuarios.size();i++){
			for(int j=0; j<propietariosProyecto.size();j++){
				if(propietariosProyecto.get(j).equals(usuarios.get(i).getEmail())){
					contadorExist++;
				}
			}
		}
		
				
		if(propietariosProyecto.size()==contadorExist){
			correoEnviado=gP.crearProyecto(propietariosProyecto, request.getParameter("nuevoProyecto"));
		}
		if(correoEnviado){
			mensaje="El proyecto " + request.getParameter("nuevoProyecto") + " se ha creado correctamente.";
			model.addObject("comienzoMensaje", "Completado. ");
			model.addObject("Mensaje", mensaje);
		}
		else{
			mensaje="No se ha podido crear su proyecto. Por favor, vuelva a intentarlo m�s tarde.";
			model.addObject("comienzoMensajeError", "Error.");
			model.addObject("MensajeError", mensaje);
		}
		return model;
	}
	

}
