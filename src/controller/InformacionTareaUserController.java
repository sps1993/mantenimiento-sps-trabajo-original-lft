package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import main.domain.DBGestionarProyectos;
import main.domain.DBGestionarTarea;
import  main.domain.DBGestionarUsuario;
import  main.domain.DatosLogin;
import  main.domain.DatosTarea;
import  main.domain.Tarea;
import  main.domain.Usuario;

@Controller

public class InformacionTareaUserController {
	
	public LinkedList<String[]> cargarTareasCompletas(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasCompletadasUser(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasActuales(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasActualesUser(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasFuturas(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasFuturasUser(tasks,email);
		
		return listaTareas;
	}
	
	@RequestMapping("volverTareasUsuario")
	public ModelAndView principal(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/vista_pantallaPrincipalUsuarios.jsp");
		DatosLogin dl = new DatosLogin();
		DBGestionarTarea gt = new DBGestionarTarea();
		DatosTarea dt= new DatosTarea();
		boolean completada = request.getParameter("completadaInformacionTarea") != null;
			String nombreTarea = dt.getNombreTarea();
			String email = dl.getEmailLogin();
			
			DBGestionarTarea tareas = new DBGestionarTarea();
			LinkedList<Tarea> tasks = new LinkedList<Tarea>();
			tasks = tareas.readTareas();
			
			for(int i=0;i<tasks.size();i++){
				if( (tasks.get(i).getnTarea().equals(nombreTarea) ) && (tasks.get(i).getEmail().equals(email) ) ){
					gt.updateTarea(tasks.get(i).getEmail(), tasks.get(i).getnTarea(), tasks.get(i).getPrioridad(), tasks.get(i).getProyecto(), completada, tasks.get(i).getFechaInicio(), tasks.get(i).getFechaFin(), tasks.get(i).getNotas());
				}
			}
		
		LinkedList<String[]> listaTareasCompletas = cargarTareasCompletas(dl.getEmailLogin());
		LinkedList<String[]> listaTareasActuales = cargarTareasActuales(dl.getEmailLogin());
		LinkedList<String[]> listaTareasFuturas = cargarTareasFuturas(dl.getEmailLogin());
		
		model.addObject("tareasCompletas",listaTareasCompletas);
		model.addObject("tareasActuales",listaTareasActuales);
		model.addObject("tareasFuturas",listaTareasFuturas);
		return model;
	}
	
	@RequestMapping("modificarTareaUsuario")
	public ModelAndView infoTarea(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model;
		DatosLogin dl = new DatosLogin();
		String email = dl.getEmailLogin();
		DatosTarea dt = new DatosTarea();
		Tarea t;
		DBGestionarTarea gt = new DBGestionarTarea();
		t=gt.readTarea(dl.getEmailLogin(), dt.getNombreTarea());
		DBGestionarProyectos gestProy=new DBGestionarProyectos();
		LinkedList<String> emails= new LinkedList <String>();
		emails.add(email);		
		LinkedList<String>proyectos=gestProy.leerProyectos(emails);
		model = new ModelAndView("/WEB-INF/jsp/ModInformacionTarea.jsp");
		model.addObject("nombreTar",t.getnTarea());
		model.addObject("prioridadTar",t.getPrioridad());
		model.addObject("proyectoTar",proyectos);
		model.addObject("completadaInformacionTarea",t.isCompletada());
		model.addObject("fInicioTar",t.getFechaInicio());
		model.addObject("fLimiteTar",t.getFechaFin());
		model.addObject("getNotasTar",t.getNotas());
		dt.setNombreTarea(t.getnTarea());
		return model;
		
	}
	
	@RequestMapping("actualizarTareaUsuario")
	public ModelAndView actualizarTarea(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model;
		boolean correoEnviado=false;
		DBGestionarTarea gt = new DBGestionarTarea();
		DatosLogin dl = new DatosLogin();
		DatosTarea dt = new DatosTarea();
		Tarea t;
		t=gt.readTarea(dl.getEmailLogin(), dt.getNombreTarea());
		String email = dl.getEmailLogin(), mensaje;	
		DBGestionarProyectos gestProy=new DBGestionarProyectos();
		LinkedList<String> emails= new LinkedList <String>();
		emails.add(email);		
		LinkedList<String>proyectos=gestProy.leerProyectos(emails);
		
		String nombre=request.getParameter("nombreTar");
		String prioridad=request.getParameter("prioridadTar");
		String proyecto=request.getParameter("proyectoTar");
		boolean completada=request.getParameter("completadaInformacionTarea") !=null;
		String fInicio=request.getParameter("fInicioTar");
		String fLimite=request.getParameter("fLimiteTar");
		String getNotas=request.getParameter("getNotasTar");
		
		if(nombre.equals("") || prioridad.equals("") || proyecto.equals("") || fInicio.equals("") || fLimite.equals("") || getNotas.equals("")){
			model = new ModelAndView("/WEB-INF/jsp/ModInformacionTarea.jsp");
			mensaje="Todos los campos deben completarse. Por favor, int�ntelo de nuevo.";
			model.addObject("comienzoMensajeError", "Error. ");
			model.addObject("MensajeError", mensaje);
		}
		else{
			gt.deleteTareaUpdate(email, dt.getNombreTarea());
			correoEnviado=gt.createTareaUpdate(email, nombre, prioridad, proyecto, completada, fInicio, fLimite, getNotas);
			
			if(correoEnviado){
				model = new ModelAndView("/WEB-INF/jsp/InformacionTarea.jsp");
				mensaje="Tarea actualizada correctamente.";
				model.addObject("comienzoMensaje", "Completado. ");
				model.addObject("Mensaje", mensaje);
			}
			else{
				model = new ModelAndView("/WEB-INF/jsp/ModInformacionTarea.jsp");
				mensaje="No se ha podido actualizar la tarea. Por favor, vuelva a intentarlo m�s tarde.";
				model.addObject("comienzoMensajeError", "Error. ");
				model.addObject("MensajeError", mensaje);
			}
		}
		
		model.addObject("nombreTar",t.getnTarea());
		model.addObject("prioridadTar",t.getPrioridad());
		model.addObject("proyectoTar",proyectos);
		model.addObject("completadaInformacionTarea",t.isCompletada());
		model.addObject("fInicioTar",t.getFechaInicio());
		model.addObject("fLimiteTar",t.getFechaFin());
		model.addObject("getNotasTar",t.getNotas());
		return model;
		
	}

}

