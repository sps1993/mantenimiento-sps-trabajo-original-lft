package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import main.domain.DBGestionarUsuario;
import main.domain.Email;
import main.domain.Usuario;

@Controller
public class RecuperarContrasenaController {
	
	@RequestMapping("/InterfazRecuperarContrasena")
	public ModelAndView darModelo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/Vista_RecuperarContrasena.jsp");
		return model;
	}
	
	@RequestMapping("/RecuperarContrase�a")
	public ModelAndView recuperar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		ModelAndView model = null;
		Email e= new Email();
		boolean enviado=false;
		String mensaje=" ";
		String email = request.getParameter("inputEmail");
		boolean continuar=true;
		DBGestionarUsuario instancia = new DBGestionarUsuario();
		//Obtenemos el usuario de la contraseña que se quiere cambiar.
		LinkedList<Usuario> lista = instancia.readUsuarios();
		
	
		while(!lista.isEmpty() && continuar){
			Usuario aux = lista.removeFirst();
			if(aux.getEmail().equals(email)){
				enviado=e.enviarEmail(email, "[TodoApp] Recuperar Contrase�a", "Contrase�a recuperada. Su contrase�a es: " + aux.getClave());
				continuar=false;
				if(enviado){
					mensaje = "Su contrase�a ha sido enviada a su correo electr�nico. Por favor, revise la Bandeja de Entrada de su correo predeterminado.";
					model = new ModelAndView("/WEB-INF/jsp/Vista_RecuperarContrasena.jsp");
					model.addObject("comienzoMensaje", "Completado.");
					model.addObject("Mensaje", mensaje);
				}
				else{
					mensaje = "La contrase�a no ha sido enviada a su correo electr�nico. Por favor, vuelva a intentarlo m�s tarde.";
					model = new ModelAndView("/WEB-INF/jsp/Vista_RecuperarContrasena.jsp");
					model.addObject("comienzoMensajeError", "Error.");
					model.addObject("MensajeError", mensaje);
				}
			}
			else{
				mensaje = "Correo no encontrado. Introduzca un correo v�lido";
				model = new ModelAndView("/WEB-INF/jsp/Vista_RecuperarContrasena.jsp");
				model.addObject("comienzoMensajeError", "Error.");
				model.addObject("MensajeError", mensaje);
			}
			
		}
		return model;
				
	}
	
	@RequestMapping("menuprincipal_home")
	public ModelAndView volverMenuprincipal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/index.jsp");
		return model;
	}
}
