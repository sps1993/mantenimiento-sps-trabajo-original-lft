package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import main.domain.DBGestionarTarea;
import main.domain.DBGestionarUsuario;
import main.domain.DatosLogin;
import main.domain.Tarea;
import main.domain.Usuario;



@Controller
public class ListarUsuarioController {
	
	@RequestMapping("Ver")
	public ModelAndView verPerfilUsuarioAdmin(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/MiPerfilAdmin.jsp");
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		boolean userEncontrado=false;
		LinkedList<String> listaUsuariosVista =usuarios.crearTablaUsuarios(users);
		
		for(int i=0; i<listaUsuariosVista.size(); i++){
			String userEmail = listaUsuariosVista.get(i);
			Usuario u=users.get(i);
			
			if(u.getEmail().equals(userEmail)) userEncontrado=true;
			
			boolean nombreBoton=request.getParameter("boton"+i) != null;
			
			if (nombreBoton && userEncontrado){
				model.addObject("email", usuarios.getEmail(u));
				model.addObject("nombre", usuarios.getNombre(u));
				model.addObject("apellidos", usuarios.getApellidos(u));
				model.addObject("direccion", usuarios.getDireccion(u));
				model.addObject("telefono", usuarios.getTelefono(u));
				model.addObject("valor", "disabled");
			}
			
		}
		
		return model;
		
	}
	
	@RequestMapping("VerTareas")
	public ModelAndView listarTareas(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/tareasUsuariosAdmin.jsp");
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		boolean userEncontrado=false;
		LinkedList<String> listaUsuariosVista =usuarios.crearTablaUsuarios(users);
		
		for(int i=0; i<listaUsuariosVista.size(); i++){
			String userEmail = listaUsuariosVista.get(i);
			Usuario u=users.get(i);
			
			if(u.getEmail().equals(userEmail)) userEncontrado=true;
			
			boolean nombreBoton=request.getParameter("boton"+i) != null;
			
			if (nombreBoton && userEncontrado){
				LinkedList<String[]> listaTareasCompletas = cargarTareasCompletas(userEmail);
				LinkedList<String[]> listaTareasActuales = cargarTareasActuales(userEmail);
				LinkedList<String[]> listaTareasFuturas = cargarTareasFuturas(userEmail);
				model.addObject("tareasCompletas",listaTareasCompletas);
				model.addObject("tareasActuales",listaTareasActuales);
				model.addObject("tareasFuturas",listaTareasFuturas);
			}
			
		}
		return model;	
	}
	
	
	
	public LinkedList<String[]> cargarTareasCompletas(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasCompletadasUser(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasActuales(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasActualesUser(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasFuturas(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasFuturasUser(tasks,email);
		
		return listaTareas;
	}
	
}
