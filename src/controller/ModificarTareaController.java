package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import main.domain.DBGestionarProyectos;
import main.domain.DBGestionarTarea;
import main.domain.DatosLogin;
import main.domain.Tarea;

@Controller
public class ModificarTareaController {
	@RequestMapping("modificartarea")
	public ModelAndView creaProyecto(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/vista_modificarTarea.jsp");
		return model;
	}
	
	@RequestMapping("guardartareamodificada")
	public ModelAndView guardar(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/vista_modificarTarea.jsp");
		
		String nombre = request.getParameter("nombre");
		String prioridad = request.getParameter("prioridad");
		String proyecto = request.getParameter("proyecto");
		String fechainicio = request.getParameter("fechainicio");
		String fechalimite = request.getParameter("fechaLimite");
		String notas = request.getParameter("notas");
		
		boolean completada = request.getParameter("completada") != null;
		
		DBGestionarTarea gestionar = new DBGestionarTarea();
		DatosLogin user = new DatosLogin();
		String email = user.getEmailLogin();
		Tarea tarea = new Tarea(email, nombre, prioridad, proyecto, fechainicio, fechalimite, notas, completada);
		gestionar.modificar(tarea, tarea);
		
		return model;
	}
	
	
}
