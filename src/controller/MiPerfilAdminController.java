package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import main.domain.DBGestionarProyectos;
import main.domain.DBGestionarTarea;
import main.domain.DBGestionarUsuario;
import main.domain.DatosLogin;
import main.domain.Usuario;



@Controller

public class MiPerfilAdminController {
	
	
	@RequestMapping("Modificar")
	public ModelAndView accesoModificar(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/jsp/ModificarPerfilAdmin.jsp");
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		DatosLogin dl = new DatosLogin();
		String email = dl.getEmailLogin();
		LinkedList<Usuario> users = usuarios.readUsuarios();
		
		String mensaje;
		int i=0;
		Usuario usuario;
		while(!(((usuario=(users.get(i))).getEmail()).equals(email))){
			i++;	
		}
		email = usuario.getEmail();
		String nombre = usuarios.getNombre(usuario);
		String apellidos = usuarios.getApellidos(usuario);
		String direccion = usuarios.getDireccion(usuario);
		String telefono = usuarios.getTelefono(usuario);
		
			//usuarios.updateUsuario(usuario.getEmail(), usuario.getClave(), nombre, apellidos, usuarios.getRol(usuario), direccion, telefono);
			
			model.addObject("nombre", nombre);
			model.addObject("email", email);
			model.addObject("apellidos", apellidos);
			model.addObject("direccion", direccion);
			model.addObject("telefono", telefono);
		return model;
	}
	
	
	@RequestMapping("EliminarCuentaAdmin")
	public ModelAndView borrarCuenta(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/index.jsp");
		DBGestionarUsuario.getInstance().desconectar();
		boolean correoEnviado;
		String mensaje, emailBorrado;
		
		DBGestionarTarea gt = new DBGestionarTarea();
		DBGestionarProyectos gp = new DBGestionarProyectos();
		DBGestionarUsuario gu = new DBGestionarUsuario();
		DatosLogin dl = new DatosLogin();
		emailBorrado=dl.getEmailLogin();
		gt.borrarTareas(emailBorrado);
		LinkedList<String>users=new LinkedList<String>();
		users.add(dl.getEmailLogin());
		gp.borrarProyectos(users);
		correoEnviado=gu.deleteUsuario(dl.getEmailLogin());
		
		if(correoEnviado){
			mensaje="El usuario " + emailBorrado + " ha sido eliminado correctamente.";
			model.addObject("comienzoMensaje", "Completado. ");
			model.addObject("Mensaje", mensaje);
		}
		else{
			mensaje="No se ha podido borrar su cuenta. Por favor, vuelva a intentarlo m�s tarde.";
			model.addObject("comienzoMensajeError", "Error.");
			model.addObject("MensajeError", mensaje);
		}
		dl.setEmailLogin("");
		return model;
	}
	
	

}
