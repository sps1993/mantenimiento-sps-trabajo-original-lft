package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import main.domain.DBGestionarUsuario;
import main.domain.DatosLogin;
import main.domain.Usuario;

@Controller
public class GestionUsuariosAdminController {

	@RequestMapping("DarDeAlta")
	public ModelAndView darDeAlta(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{

		ModelAndView model = new ModelAndView();
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		LinkedList<String[]> listaUsuariosVista =usuarios.crearTablaDadosAlta(users);
		LinkedList<String> listaCheckBox = new LinkedList<>();
		LinkedList<Object> listaCorreosEnviados = new LinkedList<Object>();
		boolean mostrarMensajeAlert=false, correoEnviado, seguir = false;
		
		for (int i=0;i<listaUsuariosVista.size();i++){
			String nombreCheckbox="checkbox"+i;
			boolean check = request.getParameter(nombreCheckbox) != null;
			String checkbox = request.getParameter(nombreCheckbox);

			if(check){
				listaCheckBox.add(checkbox+i);
				mostrarMensajeAlert=true;
			}
		}
		
		for(int k=0; k<listaCheckBox.size();k++){
			String checkBoxActual=listaCheckBox.get(k);
			String filaTabla = ""+checkBoxActual.charAt(checkBoxActual.length()-1);
			int numeroFila = Integer.parseInt(filaTabla);
				String []datosUsuario=listaUsuariosVista.get(numeroFila);
				correoEnviado=usuarios.darDeAlta(datosUsuario[0]);
				listaCorreosEnviados.add(correoEnviado);
		}
		
		for(int p=0; p<listaCorreosEnviados.size();p++){
			boolean notifCorreo=(boolean) listaCorreosEnviados.get(p);
			if(notifCorreo) seguir=true;
			else seguir=false;
		}
		
		if(seguir){
			model = new ModelAndView("/WEB-INF/jsp/DarAltas_admin.jsp");
			users =usuarios.readUsuarios();
			listaUsuariosVista =usuarios.crearTablaDadosAlta(users);
			model.addObject("lista", listaUsuariosVista);

			if(mostrarMensajeAlert){
				model.addObject("comienzoMensaje", "Completado.");
				model.addObject("Mensaje", "El/los usuario/s seleccionado/s se ha/n dado de alta correctamente.");
			}
		}
		else{
			model = new ModelAndView("/WEB-INF/jsp/DarAltas_admin.jsp");
			
			usuarios = new DBGestionarUsuario();
			users =usuarios.readUsuarios();
			
			listaUsuariosVista =usuarios.crearTablaDadosAlta(users);
			model.addObject("lista", listaUsuariosVista);
			
			if(mostrarMensajeAlert){
				model.addObject("comienzoMensajeError", "Error.");
				model.addObject("MensajeError", "El/los email/s no es/son v�lido/s. Por favor, rev�selo/s.");
			}
		}

		return model;
	}
	
	@RequestMapping("DarDeBaja")
	public ModelAndView darDeBaja(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{

		ModelAndView model = new ModelAndView();
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		LinkedList<String[]> listaUsuariosVista =usuarios.crearTablaDadosBaja(users);
		LinkedList<String> listaCheckBox = new LinkedList<>();
		LinkedList<Object> listaCorreosEnviados = new LinkedList<Object>();
		boolean mostrarMensajeAlert=false, correoEnviado, seguir = false;
		
		for (int i=0;i<listaUsuariosVista.size();i++){
			String nombreCheckbox="checkbox"+i;
			boolean check = request.getParameter(nombreCheckbox) != null;
			String checkbox = request.getParameter(nombreCheckbox);

			if(check){
				listaCheckBox.add(checkbox+i);
				mostrarMensajeAlert=true;
			}
		}
		
		for(int k=0; k<listaCheckBox.size();k++){
			String checkBoxActual=listaCheckBox.get(k);
			String filaTabla = ""+checkBoxActual.charAt(checkBoxActual.length()-1);
			int numeroFila = Integer.parseInt(filaTabla);
				String []datosUsuario=listaUsuariosVista.get(numeroFila);
				correoEnviado=usuarios.darDeBaja(datosUsuario[0]);
				listaCorreosEnviados.add(correoEnviado);
		}
		
		for(int p=0; p<listaCorreosEnviados.size();p++){
			boolean notifCorreo=(boolean) listaCorreosEnviados.get(p);
			if(notifCorreo) seguir=true;
			else seguir=false;
		}
		
		if(seguir){
			model = new ModelAndView("/WEB-INF/jsp/DarBajas_admin.jsp");
			users =usuarios.readUsuarios();
			listaUsuariosVista =usuarios.crearTablaDadosBaja(users);
			model.addObject("lista", listaUsuariosVista);

			if(mostrarMensajeAlert){
				model.addObject("comienzoMensaje", "Completado.");
				model.addObject("Mensaje", "El/los usuario/s seleccionado/s se ha/n dado de alta correctamente.");
			}
		}
		else{
			model = new ModelAndView("/WEB-INF/jsp/DarBajas_admin.jsp");
			
			usuarios = new DBGestionarUsuario();
			users =usuarios.readUsuarios();
			
			listaUsuariosVista =usuarios.crearTablaDadosBaja(users);
			model.addObject("lista", listaUsuariosVista);
			
			if(mostrarMensajeAlert){
				model.addObject("comienzoMensajeError", "Error.");
				model.addObject("MensajeError", "El/los email/s no es/son v�lido/s. Por favor, rev�selo/s.");
			}
		}

		return model;
	}
	
	@RequestMapping("CambiarRoles_Admin")
	public ModelAndView cambiarRol(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		
		ModelAndView model = new ModelAndView();
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		LinkedList<String[]> listaUsuariosVistaRol =usuarios.crearTablaRoles(users);
		LinkedList<String> listaCheckBox = new LinkedList<>();
		LinkedList<Object> listaCorreosEnviados = new LinkedList<Object>();
		boolean mostrarMensajeAlert=false, correoEnviado, seguir = false;

		String rol=request.getParameter("rol");
		
		for (int i=0;i<listaUsuariosVistaRol.size();i++){
			String nombreCheckbox="checkbox"+i;
			boolean check = request.getParameter(nombreCheckbox) != null;
			String checkbox = request.getParameter(nombreCheckbox);
			if(check){
				listaCheckBox.add(checkbox+i);
				mostrarMensajeAlert=true;
			}
		}
		
		
		for(int k=0; k<listaCheckBox.size();k++){
			String checkBoxActual=listaCheckBox.get(k);
			String filaTabla = ""+checkBoxActual.charAt(checkBoxActual.length()-1);
			int numeroFila = Integer.parseInt(filaTabla);
			String []datosUsuario=listaUsuariosVistaRol.get(numeroFila);
			correoEnviado=usuarios.cambiarRol(datosUsuario[0], rol);
			listaCorreosEnviados.add(correoEnviado);
		}
		
		for(int p=0; p<listaCorreosEnviados.size();p++){
		      boolean notifCorreo=(boolean) listaCorreosEnviados.get(p);
		      if(notifCorreo) seguir=true;
		      else seguir=false;
		}

		if(seguir){
			model= new ModelAndView("/WEB-INF/jsp/CambiarRoles_admin.jsp");
			users =usuarios.readUsuarios();
		    listaUsuariosVistaRol =usuarios.crearTablaRoles(users);
		    model.addObject("lista", listaUsuariosVistaRol);

		    if(mostrarMensajeAlert){
		      model.addObject("comienzoMensaje", "Completado.");
		      model.addObject("Mensaje", "El/los usuario/s seleccionado/s ha/n cambiado su rol correctamente.");
		    }
		}
		else{
		      model = new ModelAndView("/WEB-INF/jsp/CambiarRoles_admin.jsp");
		      
		      usuarios = new DBGestionarUsuario();
		      users =usuarios.readUsuarios();
		      
		      listaUsuariosVistaRol =usuarios.crearTablaRoles(users);
			  model.addObject("lista", listaUsuariosVistaRol);
		      
			  if(mostrarMensajeAlert){
				  model.addObject("comienzoMensajeError", "Error.");
				  model.addObject("MensajeError", "El/los email/s no es/son v�lido/s. Por favor, rev�selo/s.");
			  }
		 }

		return model;
		
	}
	
}
