package test;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarUsuario;
import main.domain.Usuario;

public class JUnitRegistroCorrecto {
	String user, pwd;
	Usuario u;
	DBGestionarUsuario gU=new DBGestionarUsuario();
	@Given("^The user is not registered in the database$")
	public void The_user_is_not_registered_in_the_database() throws Throwable {
	    user="sergio.perez1.sanchez2@gmail.com";
	    pwd="12345678";
	    u=new Usuario(user, pwd);
	    assert(!(gU.autenticar(u)));
	}

	@When("^The user gives a name and a password$")
	public void The_user_gives_a_name_and_a_password() throws Throwable {
		user="sergio.perez1.sanchez2@gmail.com";
	    pwd="12345678";
	    assert(true);
	}

	@Then("^The user will be registered$")
	public void The_user_will_be_registered() throws Throwable {
	    u=new Usuario(user, pwd);
	    gU.createUsuario(u.getEmail(), u.getClave(), "leo", "ape", "usuario", "calle", "00", false);
	    assert(gU.autenticar(u));
	}
}
