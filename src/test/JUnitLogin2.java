package test;

import static org.junit.Assert.assertFalse;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarUsuario;
import main.domain.Usuario;

public class JUnitLogin2 {

	Usuario user;
	String usuario, pwd;
	DBGestionarUsuario gU=new DBGestionarUsuario();
	@Given("^the user has registered in the system$")
	public void the_user_has_registered_in_the_system() throws Throwable {
		usuario="sergia";
		pwd="12345678";
		assert(true);
	}

	@When("^the user inserts a wrong identification$")
	public void the_user_inserts_a_wrong_identification() throws Throwable {
		user = new Usuario (usuario, pwd);
		assert(true);
	}

	@Then("^The user will not be granted access to the system$")
	public void The_user_will_not_be_granted_access_to_the_system() throws Throwable {
		assert(!(gU.autenticar(user)));
	}
}