package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarUsuario;
import main.domain.Usuario;

public class JUnitModificarUsuario {
	
	Usuario user, userN;
	DBGestionarUsuario gU=new DBGestionarUsuario();
	@Given("^The admin has a created user$")
	public void The_admin_has_a_created_user() throws Throwable {
		user = new Usuario ("sergio.perez1.sanchez2@gmail.com", "12345678");
		assert(gU.autenticar(user)); 
	}

	@When("^The admin modifies some data of this user$")
	public void The_admin_modifies_some_data_of_this_user() throws Throwable {
		userN = new Usuario ("sergio.perez1.sanchez2@gmail.com", "123456789");
		userN.setClave("12345678");
		assert(gU.modificar(userN, userN));
	}

	@Then("^The user is correctly stored in the database$")
	public void The_user_is_correctly_stored_in_the_database() throws Throwable {
		assert(gU.autenticar(userN));
	}
}
