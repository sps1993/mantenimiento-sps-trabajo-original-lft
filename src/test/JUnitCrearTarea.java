package test;

import java.util.LinkedList;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarProyectos;
import main.domain.DBGestionarTarea;
import main.domain.DBGestionarUsuario;
import main.domain.Proyecto;
import main.domain.Tarea;
import main.domain.Usuario;

public class JUnitCrearTarea {
	
	Tarea tarea;
	Usuario user;
	DBGestionarUsuario gU=new DBGestionarUsuario();
	DBGestionarProyectos gP=new DBGestionarProyectos();
	DBGestionarTarea gT= new DBGestionarTarea();
	@Given("^The user creates a task$")
	public void The_user_creates_a_task() throws Throwable {
	    user=new Usuario("sergio.perez1.sanchez2@gmail.com", "12345678");
		tarea = new Tarea (user.getEmail(),"Mandar email" ,"alta","13/01/2017","14/01/2017", "Bandeja de Entrada", "comprear globos", false);
		assert(gU.autenticar(user));
	}

	@When("^The user inserts all data$")
	public void The_user_inserts_all_datas() throws Throwable {
		assert(gT.createTarea(user.getEmail(), tarea.getnTarea(), tarea.getPrioridad(), tarea.getProyecto(), tarea.isCompletada(), tarea.getFechaInicio(), tarea.getFechaFin(), tarea.getNotas()));
	}

	@Then("^The task is in daba base$")
	public void The_task_is_in_daba_base() throws Throwable {
	    assert(gT.existeTarea(user.getEmail(), tarea.getnTarea()));
	}
}
