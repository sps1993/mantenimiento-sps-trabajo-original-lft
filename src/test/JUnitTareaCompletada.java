package test;

import java.util.LinkedList;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarProyectos;
import main.domain.DBGestionarTarea;
import main.domain.DBGestionarUsuario;
import main.domain.Proyecto;
import main.domain.Tarea;
import main.domain.Usuario;

public class JUnitTareaCompletada {
	Usuario user;
	Tarea tarea, tarea2;
	Proyecto p=new Proyecto();
	DBGestionarUsuario gU=new DBGestionarUsuario();
	DBGestionarProyectos gP=new DBGestionarProyectos();
	DBGestionarTarea gT=new DBGestionarTarea();
	@Given("^Un usuario autenticado$")
	public void Un_usuario_autenticado() throws Throwable {
		user=new Usuario("sergio.perez1.sanchez2@gmail.com", "12345678");
		assert(gU.autenticar(user));
	}

	@When("^Crea una tarea$")
	public void Crea_una_tarea() throws Throwable {
		tarea = new Tarea (user.getEmail(),"Mandar email" ,"alta","13/01/2017","14/01/2017", "Bandeja de Entrada", "comprear globos", false);
		LinkedList<String>users=new LinkedList<String>();
		users.add(user.getEmail());
		gP.leerProyecto(users, "Bandeja de Entrada");
		gT.createTarea(user.getEmail(), tarea.getnTarea(), tarea.getPrioridad(), tarea.getProyecto(), tarea.isCompletada(), tarea.getFechaInicio(), tarea.getFechaFin(), tarea.getNotas());
		assert(gT.readTarea(user.getEmail(), tarea.getnTarea()).getEmail().equalsIgnoreCase(user.getEmail()));
	}

	@Then("^Selecciona completada$")
	public void Selecciona_completada() throws Throwable {
	    tarea2=new Tarea (user.getEmail(),"Mandar email" ,"alta","13/01/2017","14/01/2017", "Bandeja de Entrada", "comprear globos", false);
	    tarea2.setCompletada(true);
	    gT.modificar(tarea, tarea2);    
	    assert(gT.readTarea(tarea2.getEmail(), tarea2.getnTarea()).isCompletada());
	}
}
