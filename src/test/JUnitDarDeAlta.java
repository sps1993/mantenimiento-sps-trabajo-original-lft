package test;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarUsuario;
import main.domain.Usuario;

public class JUnitDarDeAlta {
	Usuario admin, user, user2;
	DBGestionarUsuario gU=new DBGestionarUsuario();
	@Given("^El admin se ha autenticado$")
	public void El_admin_se_ha_autenticado() throws Throwable {
		admin=new Usuario("todoAppGrupoSPS@gmail.com","migrupomola5");
		assert(gU.autenticar(admin));
	}

	@When("^Busca a un usuario dado de baja$")
	public void Busca_a_un_usuario_dado_de_baja() throws Throwable {
		user=new Usuario ("sergio.perez1.sanchez2@gmail.com", "12345678");
		assert(gU.autenticar(user));
	}

	@Then("^Da de Alta$")
	public void Da_de_Alta() throws Throwable {
		user2=new Usuario ("sergio.perez1.sanchez2@gmail.com", "12345678");
		user2.setAlta(true);
		gU.modificar(user, user2);
		assert(gU.recuperarUsuario(user2).getAlta());
	}
}
