package test;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarUsuario;
import main.domain.Usuario;

public class JUnitCambiarRol {
	Usuario admin, user, user2;
	DBGestionarUsuario gU=new DBGestionarUsuario();
	@Given("^El administrador que esta autenticado$")
	public void El_administrador_que_esta_autenticado() throws Throwable {
		admin=new Usuario("todoAppGrupoSPS@gmail.com","migrupomola5");
		assert(gU.autenticar(admin));
	}

	@When("^Cambia de rol a un usuario$")
	public void Cambia_de_rol_a_un_usuario() throws Throwable {
		user=new Usuario ("sergio.perez1.sanchez2@gmail.com", "12345678");
		user2=new Usuario ("sergio.perez1.sanchez2@gmail.com", "12345678");
		user2.setRol("Admin");
		user2.setAlta(true);
		gU.modificar(user, user2);
	    assert(gU.recuperarUsuario(user2).getAlta()&&gU.recuperarUsuario(user2).getRol().equalsIgnoreCase("Admin"));
	}

	@Then("^Se cambia correctamente$")
	public void Se_cambia_correctamente() throws Throwable {
		assert(gU.recuperarUsuario(user2).getAlta()==(user2.getAlta()));
	}
}
