package test;

import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarUsuario;
import main.domain.Usuario;

public class JUnitCrearUsuario {

	Usuario admin, user;
	DBGestionarUsuario gU=new DBGestionarUsuario();
	@Given("^The admin creates a user$")
	public void The_admin_creates_a_user() throws Throwable {
		admin=new Usuario("todoAppGrupoSPS@gmail.com", "migrupomola5");
		assert(gU.autenticar(admin));
	}

	@When("^The admin insert all data$")
	public void The_admin_insert_all_data() throws Throwable {
	    user=new Usuario("sergiopsinformatico@gmail.com", "123456789");
	    gU.createUsuario(user.getEmail(), user.getClave(), user.getNombre(), user.getApellidos(), user.getRol(), user.getDireccion(), user.getTelefono(), user.getAlta());
	    assert(true);
	}

	@Then("^The user is in daba base$")
	public void The_user_is_in_daba_base() throws Throwable {
		assert(gU.autenticar(user));
	}
}
