package test;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarUsuario;
import main.domain.Usuario;

public class JUnitRecuperarClave {
	Usuario usuarioExiste;
	String user;
	DBGestionarUsuario gU=new DBGestionarUsuario();
	@Given("^Un usuario quiere autenticarse$")
	public void Un_usuario_quiere_autenticarse() throws Throwable {
		user = "sergio.perez1.sanchez2@gmail.com";
		assert(!(user.equals("")));
	}

	@When("^El usuario no recuerda la clave$")
	public void El_usuario_no_recuerda_la_clave() throws Throwable {
		usuarioExiste = new Usuario (user, "");
		assert(true);
	}

	@Then("^El sistema se la recuerda$")
	public void El_sistema_se_la_recuerda() throws Throwable {
	    assert(gU.recuperarClave(usuarioExiste.getEmail()).equalsIgnoreCase("12345678"));
	}
}
