package test;

import java.util.LinkedList;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarTarea;
import main.domain.DBGestionarUsuario;
import main.domain.Proyecto;
import main.domain.Tarea;
import main.domain.Usuario;

public class JUnitBorrarTarea {

	Tarea tarea;
	Usuario user;
	DBGestionarTarea gT=new DBGestionarTarea();
	DBGestionarUsuario gU=new DBGestionarUsuario();
	@Given("^The user has a  task$")
	public void The_user_has_a_task() throws Throwable {
		user=new Usuario("sergio.perez1.sanchez2@gmail.com", "12345678");
		tarea = new Tarea (user.getEmail(),"Mandar email" ,"alta","13/01/2017","14/01/2017", "Bandeja de Entrada", "comprear globos", false);
		Proyecto p=new Proyecto();
		LinkedList<String>users=new LinkedList<String>();
		users.add(user.getEmail());
		p.buscarP(users, "Bandeja de Entrada");
		gT.createTarea(user.getEmail(), tarea.getnTarea(), tarea.getPrioridad(), tarea.getProyecto(), tarea.isCompletada(), tarea.getFechaInicio(), tarea.getFechaFin(), tarea.getNotas());
		assert(gU.autenticar(user) && gT.existeTarea(user.getEmail(), tarea.getnTarea()));
	}

	@When("^The user deletes a task$")
	public void The_user_deletes_a_task() throws Throwable {
		gT.deleteTarea(tarea.getEmail(), tarea.getnTarea());
		assert(true);
	}

	@Then("^The task is not in the database$")
	public void The_task_is_not_in_the_database() throws Throwable {
		assert(!(gT.existeTarea(tarea.getEmail(), tarea.getnTarea())));
	}	
}