package test;

import java.util.LinkedList;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarProyectos;
import main.domain.DBGestionarUsuario;
import main.domain.Proyecto;
import main.domain.Usuario;

public class JUnitCompartirProyecto {
	Usuario user, user2;
	Proyecto p, pN;
	DBGestionarUsuario gU=new DBGestionarUsuario();
	DBGestionarProyectos gP=new DBGestionarProyectos();
	@Given("^Un usuario logueado$")
	public void Un_usuario_logueado() throws Throwable {
		user = new Usuario("sergio.perez1.sanchez2@gmail.com","12345678");
		assert(gU.autenticar(user));
	}

	@When("^Comparte un proyecto con otro usuario$")
	public void Comparte_un_proyecto_con_otro_usuario() throws Throwable {
		user2 = new Usuario("sergiopsinformatico@gmail.com","12345678");
		LinkedList<String>users=new LinkedList<String>();
		users.add(user.getEmail());
		p=new Proyecto();
		p.buscarP(users, "Bandeja de Entrada");
		pN=new Proyecto();
		pN.buscarP(users, "Bandeja de Entrada");
		pN.getUsuario().add(user2.getEmail());
		gP.actualizarProyecto(p, pN);
		assert(true);
	}

	@Then("^Los dos pueden crear tareas de ese proyecto$")
	public void Los_dos_pueden_crear_tareas_de_ese_proyecto() throws Throwable {
		LinkedList<String>usuarios=new LinkedList<String>();
		usuarios.add(user.getNombre());
		usuarios.add(user2.getNombre());
	    assert(p.comprobarUsuarios(usuarios, "Bandeja de Entrada"));
	}
}
