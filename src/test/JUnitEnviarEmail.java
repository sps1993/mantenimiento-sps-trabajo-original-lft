package test;

import java.util.LinkedList;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarUsuario;
import main.domain.Proyecto;
import main.domain.Usuario;

public class JUnitEnviarEmail {
	Usuario user, user2;
	Proyecto p;
	LinkedList<String>users=new LinkedList<String>();
	DBGestionarUsuario gU=new DBGestionarUsuario();
	@Given("^Un usuario que esta dentro del sistema$")
	public void Un_usuario_que_esta_dentro_del_sistema() throws Throwable {
	    user=new Usuario("todoAppGrupoSPS@gmail.com","migrupomola5");
	    user2=new Usuario("sergio.perez1.sanchez2@gmail.com","12345678");
	    assert(gU.autenticar(user) && gU.autenticar(user2));
	}

	@When("^Crea y comparte un proyecto$")
	public void Crea_y_comparte_un_proyecto() throws Throwable {
		users.add(user.getNombre());users.add(user2.getNombre());
	    p=new Proyecto(users, "Especial");
	    p.create();
	    p.delete();
	    assert(true);
	}

	@Then("^Notifica por correo a todos los usuarios el cambio$")
	public void Notifica_por_correo_a_todos_los_usuarios_el_cambio() throws Throwable {
	    assert(true);
	}
}
