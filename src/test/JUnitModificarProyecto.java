package test;

import java.util.LinkedList;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarProyectos;
import main.domain.DBGestionarUsuario;
import main.domain.Proyecto;
import main.domain.Usuario;

public class JUnitModificarProyecto {
	Usuario usuarioExiste;
	Proyecto p, pNuevo;
	DBGestionarUsuario gU=new DBGestionarUsuario();
	DBGestionarProyectos gP=new DBGestionarProyectos();
	@Given("^Un usuario identificado$")
	public void Un_usuario_identificado() throws Throwable {
		usuarioExiste = new Usuario("sergio.perez1.sanchez2@gmail.com","12345678");
		assert(gU.autenticar(usuarioExiste));
	}

	@When("^Localiza un proyecto$")
	public void Localiza_un_proyecto() throws Throwable {
		LinkedList<String>us=new LinkedList<String>();
		us.add(usuarioExiste.getEmail());
		p=new Proyecto(us,"Personal");
	    assert(gP.existeProyecto(p.getUsuario(),p.getProyecto()));
	}

	@Then("^Modifica un proyecto$")
	public void Modifica_un_proyecto() throws Throwable {
		LinkedList<String>us=new LinkedList<String>();
		us.add(usuarioExiste.getNombre());
		pNuevo=new Proyecto(us,"Personal");
		pNuevo.setProyecto("Hola");
		p.update(pNuevo);
	    assert(p.getProyecto().equalsIgnoreCase(pNuevo.getProyecto()));
	}
}
