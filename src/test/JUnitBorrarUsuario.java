package test;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarUsuario;
import main.domain.Usuario;

public class JUnitBorrarUsuario {
	Usuario admin, user, user2;
	DBGestionarUsuario gU=new DBGestionarUsuario();
	@Given("^The admin has a user$")
	public void The_admin_has_a_user() throws Throwable {
		admin=new Usuario("todoAppGrupoSPS@gmail.com","migrupomola5");
		assert(gU.autenticar(admin));
	}

	@When("^The admin deletes a user$")
	public void The_admin_deletes_a_user() throws Throwable {
		user = new Usuario ("sergio.perez1.sanchez2@gmail.com", "12345678");
	    user2=new Usuario("sergiopsinformatico@gmail.com", "123456789");
		assert(gU.autenticar(user) && gU.autenticar(user2));
	}

	@Then("^The user is not in the database$")
	public void The_user_is_not_in_the_database() throws Throwable {
		gU.deleteUsuario(user.getEmail());
		gU.deleteUsuario(user2.getEmail());
		assert(!(gU.autenticar(user) && gU.autenticar(user2)));
	}
}
