package test;

import java.util.LinkedList;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarProyectos;
import main.domain.DBGestionarUsuario;
import main.domain.Proyecto;
import main.domain.Usuario;

public class JUnitCrearProyecto {
	Usuario usuarioExiste;
	Proyecto p;
	DBGestionarUsuario gU=new DBGestionarUsuario();
	DBGestionarProyectos gP=new DBGestionarProyectos();
	@Given("^Un usuario que se ha autenticado$")
	public void Un_usuario_que_se_ha_autenticado() throws Throwable {
		usuarioExiste = new Usuario("sergio.perez1.sanchez2@gmail.com","12345678");
		assert(gU.autenticar(usuarioExiste));
	}

	@When("^Crea un proyecto$")
	public void Crea_un_proyecto() throws Throwable {
		LinkedList<String> us=new LinkedList<String>();
		us.add(usuarioExiste.getEmail());
	    p=new Proyecto(us,"Personal");
	    gP.crearProyecto(p.getUsuario(), p.getProyecto());
	    assert(true);
	}

	@Then("^Se crea correctamente$")
	public void Se_crea_correctamente() throws Throwable {
	    assert(gP.existeProyecto(p.getUsuario(), p.getProyecto()));
	}
}
