package test;

import java.util.LinkedList;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarTarea;
import main.domain.DBGestionarUsuario;
import main.domain.Proyecto;
import main.domain.Tarea;
import main.domain.Usuario;

public class JUnitTareasCaducar {
	Usuario user;
	Tarea tarea;
	DBGestionarTarea gT=new DBGestionarTarea();
	DBGestionarUsuario gU=new DBGestionarUsuario();
	@Given("^El usuario inicio sesion$")
	public void El_usuario_inicio_sesion() throws Throwable {
		user=new Usuario("sergio.perez1.sanchez2@gmail.com", "12345678");
		tarea = new Tarea ("Mandar email", user.getEmail(), "alta", "13/01/2017","14/01/2017", "Bandeja de Entrada", "comprear globos", false);
		LinkedList<String>users=new LinkedList<String>();
		users.add(user.getNombre());
		gT.createTarea(user.getEmail(), tarea.getnTarea(), tarea.getPrioridad(),tarea.getProyecto(), tarea.isCompletada(), tarea.getFechaInicio(), tarea.getFechaFin(), tarea.getNotas());
		assert(gU.autenticar(user) && gT.existeTarea(user.getEmail(), tarea.getnTarea()));
	}

	@When("^Se comprueba si alguna tarea va a caducar$")
	public void Se_comprueba_si_alguna_tarea_va_a_caducar() throws Throwable {
	    gT.avisos(user.getEmail());
	    assert(true);
	}

	@Then("^Se notifica$")
	public void Se_notifica() throws Throwable {
	    assert(true);
	}

}
