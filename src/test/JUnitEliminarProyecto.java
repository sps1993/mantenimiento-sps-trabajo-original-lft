package test;

import java.util.LinkedList;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarProyectos;
import main.domain.DBGestionarUsuario;
import main.domain.Proyecto;
import main.domain.Usuario;

public class JUnitEliminarProyecto {
	Usuario user, user2;
	Proyecto p;
	DBGestionarUsuario gU=new DBGestionarUsuario();
	DBGestionarProyectos gP=new DBGestionarProyectos();
	@Given("^Un usuario que ha iniciado sesion$")
	public void Un_usuario_que_ha_iniciado_sesion() throws Throwable {
		user = new Usuario("sergio.perez1.sanchez2@gmail.com","12345678");
		assert(gU.autenticar(user));
	}

	@When("^Busca un proyecto$")
	public void Busca_un_proyecto() throws Throwable {
		LinkedList<String>us=new LinkedList<String>();
		us.add(user.getEmail());
		p=new Proyecto(us,"Bandeja de Entrada");
	    assert(gP.existeProyecto(p.getUsuario(), p.getProyecto()));
	}

	@Then("^Elimina el proyecto$")
	public void Elimina_el_proyecto() throws Throwable {
		gP.borrarProyecto(p.getUsuario(), p.getProyecto());
	    assert(!(gP.existeProyecto(p.getUsuario(), p.getProyecto())));
	}
}
