package test;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarUsuario;
import main.domain.Usuario;

public class JUnitRegistroIncorrecto {
	Usuario usuarioExiste;
	String user, pwd;
	DBGestionarUsuario gU=new DBGestionarUsuario();
	@Given("^Un usuario se quiere registrarse$")
	public void Un_usuario_se_quiere_registrarse() throws Throwable {
		user="sergio.perez1.sanchez2@gmail.com";
		pwd="123456789";
		assert(true);
	}

	@When("^Se quiere registrar con una cuenta existente$")
	public void Se_registra_con_una_cuenta_existente() throws Throwable {
		usuarioExiste = new Usuario(user,pwd,"sergio", "ps", "usuario", "calle", "00", false );
		assert(true);
	}

	@Then("^No se registra$")
	public void No_se_registra() throws Throwable {
		boolean i=gU.createUsuario(usuarioExiste.getEmail(), usuarioExiste.getClave(), usuarioExiste.getNombre(), usuarioExiste.getApellidos(), usuarioExiste.getRol(), usuarioExiste.getDireccion(), usuarioExiste.getTelefono(), usuarioExiste.getAlta());
	    if(i)assert(false);
	    else assert(true);
	}

	@Given("^Un usuario se va a registrar$")
	public void Un_usuario_se_va_a_registrar() throws Throwable {
		user="pablo@gmail.com";
		pwd="1234";
		assert(true);
	}

	@When("^Se registra con una clave de menos de ocho caracteres$")
	public void Se_registra_con_una_clave_de_menos_de_ocho_caracteres() throws Throwable {
		usuarioExiste = new Usuario(user,pwd,"pablo", "ps", "usuario", "calle", "00", false );
		assert(true);   
	}
}