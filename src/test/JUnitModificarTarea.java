package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.LinkedList;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarProyectos;
import main.domain.DBGestionarTarea;
import main.domain.DBGestionarUsuario;
import main.domain.Proyecto;
import main.domain.Tarea;
import main.domain.Usuario;

public class JUnitModificarTarea {

	Tarea tarea, tarea2;
	Usuario usuario;
	DBGestionarUsuario gU=new DBGestionarUsuario();
	DBGestionarTarea gT=new DBGestionarTarea();
	DBGestionarProyectos gP=new DBGestionarProyectos();
	@Given("^The user has a created task$")
	public void The_user_has_a_created_task() throws Throwable {
	    usuario=new Usuario("sergio.perez1.sanchez2@gmail.com", "12345678");
		tarea = new Tarea (usuario.getEmail(),"Mandar email" ,"alta","13/01/2017","14/01/2017", "Bandeja de Entrada", "comprear globos", false);
		tarea2 = new Tarea (usuario.getEmail(),"Mandar email" ,"alta","13/01/2017","14/01/2017", "Bandeja de Entrada", "comprear globos", false);
		assert(gU.autenticar(usuario)&& gT.existeTarea(usuario.getEmail(), tarea.getnTarea()));
	}

	@When("^The user modifies some data of this task$")
	public void The_user_modifies_some_data_of_this_task() throws Throwable {
		tarea2.setFechaFin("08/01/2017");
		LinkedList<String>users=new LinkedList<String>();
		users.add(usuario.getEmail());
		gP.existeProyecto(users, "Bandeja de Entrada");
		gT.modificar(tarea, tarea2);
		assert(true);
	}

	@Then("^The task is correctly stored in the database$")
	public void The_task_is_correctly_stored_in_the_database() throws Throwable {
		assert(gT.readTarea(tarea2.getEmail(), tarea2.getnTarea()).getFechaFin().equalsIgnoreCase(tarea2.getFechaFin()));
	}
}