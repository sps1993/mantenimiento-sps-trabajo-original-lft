package test;

import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.DBGestionarUsuario;
import main.domain.Usuario;

public class JUnitLogin {

	Usuario user;
	String usuario, pwd;
	DBGestionarUsuario gU=new DBGestionarUsuario();
	@Given("^The user has registered in the system$")
	public void The_user_has_registered_in_the_system() throws Throwable {
		usuario="todoAppGrupoSPS@gmail.com";
		pwd="migrupomola5";
		assert(true);
	}

	@When("^The user inserts a valid identification$")
	public void The_user_inserts_a_valid_identification() throws Throwable { 
		user = new Usuario (usuario, pwd);
		assert(true); 
	}

	@Then("^The user will be granted access to the system$")
	public void The_user_will_be_granted_access_to_the_system() throws Throwable {
		assert(gU.autenticar(user));
	}
}
